<div class="sidebar-wrapper" data-simplebar="true">
   <div class="sidebar-header">
      <div>
         <img src="{{asset('backend/logo.png')}}" class="logo-icon" alt="logo icon">
      </div>
   
      <div class="toggle-icon ms-auto"><i class='bx bx-arrow-to-left'></i>
      </div>
   </div>

   <!--navigation-->
   <ul class="metismenu" id="menu">
      <li>
         <a href="{{ route('admin.dashboard') }}">
            <div class="parent-icon"><i class='bx bx-home-circle'></i>
            </div>
            <div class="menu-title">Dashboard</div>
         </a>
      </li>

      <hr>

      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Manage Exporter</div>
         </a>
         <ul>
            <li> <a href="{{ route('view_exporter') }}"><i class="bx bx-right-arrow-alt"></i>Add Exporter</a></li>
         </ul>
      </li>

      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Manage Importer</div>
         </a>
         <ul>
            <li> <a href="{{ route('view_importer') }}"><i class="bx bx-right-arrow-alt"></i>Add Importer</a></li>
         </ul>
      </li>

      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Manage Category</div>
         </a>
         <ul>
            <li> <a href="{{ route('view_category') }}"><i class="bx bx-right-arrow-alt"></i>Add Category</a></li>
         </ul>
      </li>

      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Manage Expense</div>
         </a>
         <ul>
            <li> <a href="{{ route('view_expense') }}"><i class="bx bx-right-arrow-alt"></i>Add Expense</a></li>
         </ul>
      </li>

      <hr>

      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Manage Export Bill</div>
         </a>
         <ul>
            <li> <a href="{{ route('add_export_bill') }}"><i class="bx bx-right-arrow-alt"></i>Add New Export Bill</a></li>

            <li> <a href="{{ route('view_export_bill') }}"><i class="bx bx-right-arrow-alt"></i>Export Bill List</a></li>
         </ul>
      </li>


      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Manage Import Bill</div>
         </a>
         <ul>
            <li> <a href="{{ route('add_import_bill') }}"><i class="bx bx-right-arrow-alt"></i>Add New Import Bill</a></li>

            <li> <a href="{{ route('view_import_bill') }}"><i class="bx bx-right-arrow-alt"></i>Import Bill List</a></li>
         </ul>
      
         <ul>
            <li> <a href="{{ route('add_bitumin_import_bill') }}"><i class="bx bx-right-arrow-alt"></i>Add New Bitumin Import Bill</a></li>

            <li> <a href="{{ route('view_bitumin_import_bill') }}"><i class="bx bx-right-arrow-alt"></i>Bitumin Import Bill List</a></li>
         </ul>
      </li>

      <hr>
      
      <li>
         <a href="javascript:;" class="has-arrow">
            <div class="parent-icon"><i class="bx bx-category"></i>
            </div>
            <div class="menu-title">Manage Forwarding Bill</div>
         </a>
         <ul>
            <li> <a href="{{ route('add_export_forwarding_bill') }}"><i class="bx bx-right-arrow-alt"></i>Export Forwarding Bill</a></li>
            <li> <a href="{{ route('view_export_forwarding_bill') }}"><i class="bx bx-right-arrow-alt"></i>Export Forwarding Bill List</a></li>
         </ul>
         <!-- <ul>
            <li> <a href="{{ route('add_import_forwarding_bill') }}"><i class="bx bx-right-arrow-alt"></i>Import Forwarding Bill</a></li>
            <li> <a href="{{ route('view_import_forwarding_bill') }}"><i class="bx bx-right-arrow-alt"></i>Import Forwarding Bill List</a></li>
         </ul> -->
         <ul>
            <li> <a href="{{ route('add_bitumin_forwarding_bill') }}"><i class="bx bx-right-arrow-alt"></i>Bitumin Forwarding Bill</a></li>
            <li> <a href="{{ route('view_bitumin_forwarding_bill') }}"><i class="bx bx-right-arrow-alt"></i>Bitumin Import Forwarding Bill List</a></li>
         </ul>
         <ul>
            <li> <a href="{{ route('add_benapole_forwarding_bill') }}"><i class="bx bx-right-arrow-alt"></i>Benapole Import Forwarding Bill</a></li>
            <li> <a href="{{ route('view_benapole_forwarding_bill') }}"><i class="bx bx-right-arrow-alt"></i>Benapole Import Forwarding Bill List</a></li>
         </ul>
      </li>

      <hr>

      <!-- <li>
         <a href="https://webbysys.com/" target="_blank">
            <div class="parent-icon"><i class="bx bx-support"></i>
            </div>
            <div class="menu-title">Support</div>
         </a>
      </li> -->
      <br><br><br><br><br><br>
   </ul>
   <!--end navigation-->
</div>