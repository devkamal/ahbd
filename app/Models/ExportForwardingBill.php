<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExportForwardingBill extends Model
{
    use HasFactory;
    protected $guarded = [];


    public function exporter()
    {
        return $this->belongsTo(Exporter::class);
    }

    public function details()
    {
        return $this->hasMany(ExportForwardingBillDetail::class);
    }
    
}
