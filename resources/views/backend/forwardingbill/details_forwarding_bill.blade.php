@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <div class="row">
         <div class="col-xl-12 mx-auto">
           <div class="d-flex justify-content-between">
                <h6 class="mb-0 text-uppercase">Details Export Forwarding Bill</h6>
                <h6><a href="{{ route('view_export_forwarding_bill') }}">Back</a></h6>
           </div>
            <hr/>
            <div class="card">
               <div class="card-body">
                  <div class="p-4 border rounded">
                     <form action="{{ route('update_export_bill',$details->id) }}" class="row g-3 needs-validation" method="post" novalidate>
                        @csrf
                        <div class="col-md-4">
                           <label for="exporter_id" class="form-label">Exporter name <span class="text-danger">*</span></label>
                           <select onchange="getAddress()" class="form-select" readonly required class="form-control">
                              @foreach ($exporters as $item)
                                 <option value="{{ $item->id }}" {{ $item->id == $details->exporter_id ? 'selected' : ''}}>{{ $item->exporter_name }}</option>
                              @endforeach
                           </select>
                        </div>
                        <div class="col-md-4">
                           <label for="address" class="form-label">Exporter Address <span class="text-danger">*</span></label>
                           <input type="text" class="form-control" readonly value="{{ $details->exporter_address }}" required>
                        </div>
                        <div class="col-md-4">
                           <label for="ip_no" class="form-label">Bill To<span class="text-danger">*</span></label>
                           <input type="text" class="form-control" readonly value="{{ $details->bill_to }}" required>
                        </div>
                 
                        <div class="col-md-4">
                           <label for="sub" class="form-label">Subject <span class="text-danger">*</span></label>
                           <input type="text" class="form-control" readonly value="{{ $details->sub }}" required>
                        </div>

                        <div class="col-md-4">
                           <label for="message" class="form-label">Message<span class="text-danger">*</span></label>
                           <input type="text" class="form-control" readonly value="{{ $details->message }}" required>
                        </div>
                        <div class="col-md-3">
                           <label for="grand_total" class="form-label"> Total Amount<span class="text-danger">*</span></label>
                           <input type="text" class="form-control" readonly value="{{ $details->grand_total }}" required>
                        </div>
                        
                        <hr>
                    

                          <!-------------Start calculation table------------------->     
                        <table class="table table-bordered" id="dynamic-table">
                              <thead class="bg-dark">
                                    <tr class="text-white text-center">
                                       <th scope="col">SL/No</th>
                                       <th scope="col">Bill No </th>
                                       <th scope="col"> Date</th>
                                       <th scope="col">Invoice No</th>
                                       <th scope="col">Invoice Date</th>
                                       <th scope="col">Bill Amount (TK)</th>
                                    </tr>
                              </thead>
                              @foreach ($details->details as $key => $expense)
                                    <tr class="text-center">
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $expense->bill_no }} </td>
                                    <td>{{ \Carbon\Carbon::parse($expense->created_at)->format('d-m-Y')}} </td>
                                    <td> {{ $expense->invoice_no }}</td>
                                    <td> {{ $expense->invoice_date }}</td>
                                    <td>{{ accounting_format($expense->amount) }}</td>
                                    </tr>
                              @endforeach
                              <tbody>
                                 <tr>
                                    <td style="text-align:right" colspan="5"><b> Total Amount:</b></td>
                                    <td class="text-center"><b>{{ accounting_format($details->grand_total) }}</b></td>
                                 </tr>
                              </tbody>
                           </table>                     
                            <!-------------End calculation table------------------->                                  
                                
                            </div>
                         
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection