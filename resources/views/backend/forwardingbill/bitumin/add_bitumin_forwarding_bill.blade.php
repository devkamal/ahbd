@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <div class="row">
         <div class="col-lg-12">
            <div class="card mb-4">
               <div class="card text-left">
                  <div class="card-body">
                     <h4 class="card-title mb-3">Add New Bitumin Forwarding Bill</h4>
                     <form action="{{ route('store_bitumin_forwarding_bill') }}" method="POST">
                        @csrf
                        <div class="row">
                           <div class="col-lg-4">
                              <table class="table table-bordered">
                                 <tbody>
                                    <tr>
                                       <td colspan="2">
                                          <select name="importer_id" id="importer_id" class="form-control" onchange="getImporter()">
                                             <option value="">Select Importer</option>
                                             @foreach ($importers as $item)
                                             <option value="{{ $item->id }}">{{ $item->importer_name }}</option>
                                             @endforeach
                                          </select>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Bill To</td>
                                       <td>
                                          <input type="text" name="bill_to" id="bill_to" class="form-control">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Importer Name</td>
                                       <td>
                                          <input type="text" name="importer_name" id="importer_name" class="form-control" placeholder="Exporter Name">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Importer Address</td>
                                       <td>
                                          <div class="form-group">
                                             <input type="text" class="form-control" name="importer_address" id="importer_address" placeholder="address" pwa2-uuid="EDITOR/input-4F3-D08-7BBBF-5C2" pwa-fake-editor="">
                                          </div>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                           <div class="col-lg-4"></div>
                           <div class="col-lg-4"></div>
                        </div>
                        <div>
                        <span>Sub: </span>
                        <input type="text" name="sub" class="form-control" id="sub" value="Submission of C&F Bill No:02"> <br>
                        <span>Message (Body): </span>
                        <input type="text" name="message" class="form-control" id="message" 
                        value="We are submitting here with the following C&F Bill and in this connection we would also request you to kindly
                        arrage payment of the same at an early date from your end"> <br>

                           <table class="table table-bordered">
                              <thead class="bg-dark">
                                 <tr class="text-white text-center">
                                    <th scope="col">Sl</th>
                                    <th scope="col" style="width:20%">Bill No</th>
                                    <th scope="col" style="width:20%"> Date</th>
                                    <th scope="col">LC No</th>
                                    <th scope="col">Particulars</th>
                                    {{--<th scope="col">Buyer</th>--}}
                                    <th scope="col">Amount</th>
                                    <th scope="col">Actions</th>
                                 </tr>
                              </thead>
                              <tbody id="table-body">
                                 <tr>
                                    <td class="text-center">1</td>
                                    <td>
                                       <select name="bill_no[]" id="bill_no" class="form-control select2" onchange="getAmount(this)">
                                          <option value=""></option>
                                          @foreach ($importBills as $item)
                                          <option value="{{ $item->id }}">{{ $item->bill_no}} </option>
                                          @endforeach
                                       </select>
                                    </td>

                                    <td>
                                       <input type="date" name="date[]" id="date" class="form-control">
                                    </td>
                                    
                                    <td>
                                       <!-- <input type="text" name="bl_no[]" class="form-control"> -->
                                       <input type="text" name="lc_no[]" id="lc_no" class="form-control">
                                    </td>
                                    <td class="text-center">
                                       <!-- <input type="text" name="ip_no[]" class="form-control"> -->
                                       <input type="text" name="consignment_of[]" id="consignment_of" class="form-control">
                                    </td>

                                    {{--<td class="text-center">
                                       <input type="text" name="buyer[]" class="form-control">
                                    </td>--}}

                                    <td class="text-center">
                                       <input type="text" name="amount[]" id="amount" class="form-control">
                                    </td>
                                    <td class="d-flex">
                                       <div class="btn btn-primary" onclick="addRow()">+</div>
                                       <div class="btn btn-danger" onclick="removeRow(this)">-</div>
                                    </td>
                                 </tr>
                              </tbody>
                              <tbody>
                                 <tr>
                                    <td colspan="5" style="text-align:right"> Total Amount:</td>
                                    <td id="grand_total_cell">
                                       <span id="grand_total">0</span>
                                       <input type="hidden" name="grand_total" id="grand_total_input" value="0">
                                    </td>
                                 </tr>
                              </tbody>

                           </table>
                           <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>



<script>
   //Get importer ways billTo, name, address
   function getImporter() {
       var id = $('#importer_id').val();
       $.ajax({
           url: '/import-forwarding-bill/get-importer/' + id,
           type: 'GET'
       })
       .done(function(response) {
           $('#bill_to').val(response.bill_to);
           $('#importer_name').val(response.importer_name);
           $('#importer_address').val(response.importer_address);
       })
       .fail(function(jqXHR, textStatus, errorThrown) {
           console.error('Error fetching importer details:', textStatus, errorThrown);
       });
   }
   
     //Get bill no ways get amount
     function getAmount(selectElement) {
       var id = $(selectElement).val();
       $.ajax({
           url: '/import-forwarding-bill/get-grand-total/' + id,
           type: 'GET'
       })
       .done(function(response) {
           var date = response.date;
           var lc_no = response.lc_no;
           var consignment_of = response.consignment_of;
           var amount = response.amount || 0;
           $(selectElement).closest('tr').find('input[name="date[]"]').val(date);
           $(selectElement).closest('tr').find('input[name="lc_no[]"]').val(lc_no);
           $(selectElement).closest('tr').find('input[name="consignment_of[]"]').val(consignment_of);
           $(selectElement).closest('tr').find('input[name="amount[]"]').val(amount);
           updateGrandTotal();
       })
       .fail(function(jqXHR, textStatus, errorThrown) {
           console.error('Error fetching grand total:', textStatus, errorThrown);
       });
   }
   
   function addRow() {
       var tableBody = $('#table-body');
       var newRow = $('<tr></tr>');
   
       newRow.append('<td class="text-center">' + (tableBody.children('tr').length + 1) + '</td>');
       newRow.append('<td>' + generateImportBillsOptions() + '</td>');
       newRow.append('<td><input type="text" name="date[]" class="form-control"></td>');
       newRow.append('<td><input type="text" name="lc_no[]" class="form-control"></td>');
       newRow.append('<td class="text-center"><input type="text" name="consignment_of[]" class="form-control"></td>');
      //  newRow.append('<td class="text-center"><input type="text" name="buyer[]" class="form-control"></td>');
       newRow.append('<td class="text-center"><input type="text" name="amount[]" class="form-control"></td>');
       newRow.append('<td class="d-flex"><div class="btn btn-primary" onclick="addRow()">+</div><div class="btn btn-danger" onclick="removeRow(this)">-</div></td>');
   
       tableBody.append(newRow);
       newRow.find('.select2').select2();
       updateAmounts();
       updateGrandTotal();
   }
   
   
   function updateAmounts() {
       $('#table-body tr').each(function () {
           var row = $(this);
           var billNo = row.find('select[name="bill_no[]"]').val();
           var amountInput = row.find('input[name="amount[]"]');
           getAmount(billNo, amountInput);
       });
   }
   
   function generateImportBillsOptions() {
   var optionsHtml = '<select name="bill_no[]" class="form-control select2" onchange="getAmount(this)">';
   optionsHtml += '<option value=""></option>';
   @foreach ($importBills as $item)
       optionsHtml += '<option value="{{ $item->id }}">{{ $item->bill_no }} / {{ \Carbon\Carbon::parse($item->date)->format('d/m/Y')}}</option>';
   @endforeach
   optionsHtml += '</select>';
   return optionsHtml;
   }
   
   
   function removeRow(button) {
       var rowToRemove = $(button).closest('tr');
       rowToRemove.remove();
       updateGrandTotal();
   }
   

   function updateGrandTotal() {
   var grandTotalSum = 0;

   $('#table-body tr').each(function () {
      var amountInput = $(this).find('input[name="amount[]"]');
      var amount = parseFloat(amountInput.val()) || 0;
      grandTotalSum += amount;
   });

   $('#grand_total').text(grandTotalSum.toFixed(2));
   $('#grand_total_input').val(grandTotalSum.toFixed(2));
}

</script>


@endsection