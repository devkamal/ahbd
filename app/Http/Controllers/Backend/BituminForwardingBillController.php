<?php

namespace App\Http\Controllers\Backend;

use App\Models\Category;
use App\Models\Importer;
use App\Models\ImportBill;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BituminForwardingBill;
use App\Models\BituminForwardingBillDetail;

class BituminForwardingBillController extends Controller
{
    public function view(){
        $allData = BituminForwardingBill::orderBy('id','desc')->paginate(10);
        return view('backend.forwardingbill.bitumin.view_bitumin_forwarding_bill',compact('allData'));
       }
    
    public function add(){
    $importers = Importer::all();
    $importBills = ImportBill::all();
    return view('backend.forwardingbill.bitumin.add_bitumin_forwarding_bill',compact('importers','importBills'));
    }


    public function store(Request $request)
    {
        //dd($request->all());

        $bituminForwardingBill = BituminForwardingBill::create([
            'importer_id' => $request->input('importer_id'),
            'bill_to' => $request->input('bill_to'),
            'importer_name' => $request->input('importer_name'),
            'importer_address' => $request->input('importer_address'),
            'sub' => $request->input('sub'),
            'message' => $request->input('message'),
            'grand_total' => $request->input('grand_total'),
        ]);

        // Loop through the details and create import Forwarding Bill Details
        foreach ($request->input('bill_no') as $index => $billNo) {
            BituminForwardingBillDetail::create([
                'bitumin_forwarding_bill_id' => $bituminForwardingBill->id,
                'bill_no' => $billNo,
                'lc_no' => $request->input('lc_no')[$index],
                'consignment_of' => $request->input('consignment_of')[$index],
                // 'buyer' => $request->input('buyer')[$index],
                'amount' => $request->input('amount')[$index],
            ]);
        }

        $notification = array(
        'message' => 'BituminForwardingBill Inserted Successfully',
            'alert-type' => 'success'
        );

        return redirect()->route('view_bitumin_forwarding_bill')->with($notification); 
      }
    
      public function edit($id){
        $editData = BituminForwardingBill::with('details')->findOrFail($id);
        $categories = Category::all();
        $importers = Importer::all();
        $importBills = ImportBill::all();
        return view('backend.forwardingbill.bitumin.edit_bitumin_forwarding_bill',
        compact('importers','categories','importBills','editData'));
       }

       public function update(Request $request, $id){
        $data = BituminForwardingBill::find($id);
        $data->importer_id = $request->importer_id;
        $data->bill_to   = $request->bill_to;
        $data->importer_name = $request->importer_name;
        $data->importer_address = $request->importer_address;
        $data->sub = $request->sub;
        $data->message = $request->message;
        $data->update();
        
        $notification = array(
            'message' => 'BituminForwardingBill update successfully!',
            'alert-type' => 'success',
        );
        return redirect()->route('view_bitumin_forwarding_bill')->with($notification);

       }


       public function detailsView($id){
        $details = BituminForwardingBill::with('details')->findOrFail($id);
        $importers = Importer::all();
        return view('backend.forwardingbill.bitumin.details_forwarding_bill',compact('details','importers'));
       }

       public function pdfBituminForwardingBill($id){
        $pdfbill = BituminForwardingBill::with('details')->findOrFail($id);
        $importers = Importer::all();
        return view('backend.forwardingbill.bitumin.pdf_forwarding_importbill',compact('pdfbill','importers'));
       }
       

    
    public function delete($id){
       $product = BituminForwardingBill::findOrFail($id);
       BituminForwardingBill::findOrFail($id)->delete();
    
       $details = BituminForwardingBillDetail::where('bitumin_forwarding_bill_id',$id)->get();
       foreach($details as $item){
           BituminForwardingBillDetail::where('bitumin_forwarding_bill_id',$id)->delete();
       }
    
       $notification = array(
           'message' => 'Bitumin Forwarding Bill Deleted Successfully',
           'alert-type' => 'success'
       );
    
       return redirect()->back()->with($notification);
    }

    //GEt json data from importer like name,address,billto
    public function GetImporter($id){
        $importer = Importer::find($id);

        if ($importer) {
            return response()->json([
                'bill_to' => $importer->bill_to, 
                'importer_name' => $importer->importer_name, 
                'importer_address' => $importer->importer_address
            ]);
        } else {
            return response()->json(['error' => 'Importer not found'], 404);
        }
    }

    public function GetGrandTotal($id){
        $grandTotal = ImportBill::find($id);

        if ($grandTotal) {
            return response()->json([
                'amount' => $grandTotal->grand_total,
            ]);
        } else {
            return response()->json(['error' => 'GrandTotal not found'], 404);
        }
    }
}
