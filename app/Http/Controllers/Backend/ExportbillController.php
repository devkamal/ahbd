<?php

namespace App\Http\Controllers\Backend;

use App\Models\Category;
use App\Models\Exporter;
use App\Models\ExportBill;
use Illuminate\Http\Request;
use App\Models\ExpenseDetails;
use App\Http\Controllers\Controller;

class ExportbillController extends Controller
{
       public function view(){
        $allData = ExportBill::orderBy('id','desc')->paginate(10);
        return view('backend.exportbill.view_exportbill',compact('allData'));
       }
    
       public function add(){
        $categories = Category::all();
        $exporters = Exporter::all();
        return view('backend.exportbill.add_exportbill',compact('exporters','categories'));
       }
       

       public function store(Request $request)
       {
            //dd($request->all());
          
            $validateData = $request->validate([
                'date' => 'required',
                'exporter_id' => 'required',
                'category_id' => 'required',
            ]);

            $exportBill = ExportBill::create([
               'date' => $request->date,
               'exporter_id' => $request->exporter_id,
               'exporter_address' => $request->exporter_address,
               'ip_no' => $request->ip_no,
               'invoice_date' => $request->invoice_date,
               'invoice_no' => $request->invoice_no,
               'consignment_of' => $request->consignment_of,
               'be_date' => $request->be_date,
               'be_no' => $request->be_no,
               'lc_date' => $request->lc_date,
               'lc_no' => $request->lc_no,
               'rot_no' => $request->rot_no,
               'export_ss' => $request->export_ss,
               'bl_no' => $request->bl_no,
               'bl_date' => $request->bl_date,
               'category_id' => $request->category_id,
               'bill_no' => $request->bill_no,

               'usd_value' => $request->usd_value,
               'bdt_rate' => $request->bdt_rate,
               'invoice_value' => $request->invoice_value,

               'sub_total' => $request->sub_total,
               'grand_total' => $request->grand_total,

               'agency_fixed' => $request->agency_fixed,
               'agency_value' => $request->agency_value,
               'agency_percent' => $request->agency_percent,
           ]);
   
           // Create ExpenseDetails
           $expenseDetails = [];
           foreach ($request->expense_name as $key => $expense_name) {
               $expenseDetails[] = new ExpenseDetails([
                   'expense_name' => $expense_name,
                   'description' => $request->description[$key],
                //    'unit' => $request->unit[$key],
                   'rate' => $request->rate[$key],
                   'qty' => $request->qty[$key],
                   'amount' => $request->amount[$key],
               ]);
           }
   
           // Associate ExpenseDetails with ExportBill
           $exportBill->expenseDetails()->saveMany($expenseDetails);
   
          
         $notification = array(
            'message' => 'ExportBill Inserted Successfully',
               'alert-type' => 'success'
         );
   
         return redirect()->route('view_export_bill')->with($notification); 
       }

    
       public function edit($id){
         $editData = ExportBill::with('expenseDetails')->findOrFail($id);
         $exporters = Exporter::all();
         $categories = Category::all();
         return view('backend.exportbill.edit_exportbill',compact('editData','exporters','categories'));
       }

       public function detailsView($id){
        $details = ExportBill::with('expenseDetails')->findOrFail($id);
        $exporters = Exporter::all();
        $categories = Category::all();
        return view('backend.exportbill.details_exportbill',compact('details','exporters','categories'));
       }

       public function pdfExportBill($id){
        $pdfbill = ExportBill::with('expenseDetails')->findOrFail($id);
        $exporters = Exporter::all();
        $categories = Category::all();
        return view('backend.exportbill.pdf_exportbill',compact('pdfbill','exporters','categories'));
       }
       
    
       public function update(Request $request, $id)
       {

           $exportBill = ExportBill::findOrFail($id);
       
           $exportBill->update([
               'date' => $request->date,
               'exporter_id' => $request->exporter_id,
               'exporter_address' => $request->exporter_address,
               'ip_no' => $request->ip_no,
               'invoice_date' => $request->invoice_date,
               'invoice_no' => $request->invoice_no,
               'consignment_of' => $request->consignment_of,
               'be_date' => $request->be_date,
               'be_no' => $request->be_no,
               'lc_date' => $request->lc_date,
               'lc_no' => $request->lc_no,
               'rot_no' => $request->rot_no,
               'export_ss' => $request->export_ss,
               'bl_no' => $request->bl_no,
               'bl_date' => $request->bl_date,
               'category_id' => $request->category_id,
               'bill_no' => $request->bill_no,

               'usd_value' => $request->usd_value,
               'bdt_rate' => $request->bdt_rate,
               'invoice_value' => $request->invoice_value,
       
               'sub_total' => $request->sub_total,
               'grand_total' => $request->grand_total,
       
               'agency_fixed' => $request->agency_fixed,
               'agency_value' => $request->agency_value,
               'agency_percent' => $request->agency_percent,
           ]);
       
           // Update or create ExpenseDetails
           $expenseDetails = [];
           foreach ($request->description as $key => $description) {
               $expenseDetails[] = [
                   'expense_name' => $request->expense_name[$key],
                   'description' => $request->description[$key],
                   'unit' => $request->unit[$key],
                   'rate' => $request->rate[$key],
                   'qty' => $request->qty[$key],
                   'amount' => $request->amount[$key],
               ];
           }
       
           $exportBill->expenseDetails()->delete(); // Delete existing records
           $exportBill->expenseDetails()->createMany($expenseDetails);
       
           $notification = [
               'message' => 'ExportBill Updated Successfully',
               'alert-type' => 'success',
           ];
       
           return redirect()->route('view_export_bill')->with($notification);
    }
       
       
    
    public function delete($id){
       $product = Exportbill::findOrFail($id);
       Exportbill::findOrFail($id)->delete();
    
       $details = ExpenseDetails::where('export_bill_id',$id)->get();
       foreach($details as $item){
           ExpenseDetails::where('export_bill_id',$id)->delete();
       }
    
       $notification = array(
           'message' => 'Export Bill Deleted Successfully',
           'alert-type' => 'success'
       );
    
       return redirect()->back()->with($notification);
    }
    
}
