<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chowdhury Syndicate C&F Bill</title>
    <style>
      @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

        body {
            font-family: 'Poppins', sans-serif;
            margin: 20px;
        }

        h1 {
            color: #201D1D;
            font-size: 1.5rem;
            font-weight: 600;
        }

        .addresses {
            margin-top: 0.5rem;
        }

        .addresses p span {
            color: #201D1D;
            font-size: 0.75rem;
            font-weight: 600;
        }

        .addresses p {
            color: #474747;
            font-size: 0.75rem;
            font-weight: 400;
        }

        .addresses p:nth-child(1) {
            color: #201D1D;
            font-size: 0.75rem;
            font-weight: 500;
            text-transform: uppercase;
        }

        .dates {
            margin-top: 2.5rem;
        }

        .dates p {
            color: #201D1D;
            font-size: 0.75rem;
            font-weight: 400;
            line-height: 1.375rem;
        }

        .dates p span {
            color: #474747;
            font-size: 0.75rem;
            font-weight: 400;
        }

        .sub {
            margin-top: 1.19rem;
        }

        .sub p span {
            color: #474747;
            font-size: 0.75rem;
            font-weight: 400;
        }

        .sub p  {
            color: #201D1D;
            font-size: 0.75rem;
            font-weight: 600;
        }

        .paragraph {
            margin-top: 1.31rem;
        }

        .paragraph p {
            color: #474747;
            font-size: 0.75rem;
            font-weight: 400;
            line-height: 1.375rem;
        }

        .paragraph p span {
            color: #201D1D;
            font-size: 0.75rem;
            font-weight: 500;
            line-height: 1.375rem;
        }


        p {
            margin: 0;
        }

        .spacer {
            margin-bottom: 20px;
        }

        table {
            margin-top: 2.5rem;
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        th {
            color: #201D1D;
            text-align: center;
            font-size: 0.75rem;
            font-weight: 500;
        }
        table td{
                font-weight: 600;
            }

        td {
            color: #474747;
            font-size: 0.75rem;
            font-weight: 400;
        }

        th, td {
            border: 1px solid #ddd;
            padding: 10px;
            text-align: left;
        }

        th {
            background-color: #F3F3F3;
        }

        .grand-total {
            display: flex;
            justify-content: flex-end;
            gap: 2rem;
            margin-top: 10px;
            text-align: end;
            margin-right: 4rem;
            
        }

        .grand-total .price {
            font-weight: bold;
        }

        .thanking {
            margin-top: 2.5rem;
        }

        .thanking p {
            color: #474747;
            font-size: 0.75rem;
            font-weight: 400;
            line-height: 1.375rem;
        }

        .thanking-bottom {
            margin-top: 1.25rem;
        }


        @media print {
            body {
                margin: 1cm;
            }

            table {
                page-break-inside: avoid;
            }

            .hide-in-print {
                display: none;
            }
        }
    </style>
</head>
<body onload="window.print()">
    <h1>Chowdhury Syndicate</h1>

    <div class="addresses">
        <p>Shipping Clearing & Forwarding Agent</p>
        <p><span>Phone:</span> 031-2858488,631613,622405</p>
        <p><span>Location:</span> 950/B,AsadGonj,Chittagong,</p>
    </div>

    <div class="dates">
        <p>Date: <span>{{ \Carbon\Carbon::parse($pdfbill->created_at)->format('d-m-Y')}}</span></p>
        <p>{{ $pdfbill['exporter']['exporter_name'] }} </p>
        <p>{{ $pdfbill['exporter']['exporter_address'] }} </p>
    </div>

    <div class="sub">
        <p><span>Sub:</span> {{ $pdfbill->sub }}</p>
    </div>

    <div class="paragraph">
        <p>Dear <span>Sir,</span><br>
            {{ $pdfbill->message }}
        </p>
    </div>

    <table>
        <tr>
            <th>SL/No</th>
            <th>Bill No</th>
            <th>Date</th>
            <th>Invoice No</th>
            <th>Invoice Date</th>
            <th>Bill Amount (Tk)</th>
        </tr>
        @foreach ($pdfbill->details as $key => $expense)
               <tr class="text-center">
               <td>{{ $key+1 }}</td>
               <td>{{ $expense->bill_no }} </td>
               <td>{{ \Carbon\Carbon::parse($expense->created_at)->format('d-m-Y')}}</td>
               <td> {{ $expense->invoice_no }} </td>
               <td> {{ $expense->invoice_date }}</td>
               <td>{{ accounting_format($expense->amount) }}</td>
               </tr>
         @endforeach
    </table>

    <div class="grand-total">
        <p style="font-size:10px;margin-top:5px">Amount in Words : {{numberToWord($pdfbill->grand_total)}}</p>
        <p>Total Amount</p>
        <p class="price">{{ accounting_format($pdfbill->grand_total) }}</p>
    </div>

    <div class="thanking">
        <div class="thanking-top">
            <p>Thanking You,<br>Your's faithfully,</p>
        </div>
        <div class="thanking-bottom">
            <p>
                Chowdhury Syndicate
            </p>
        </div>
    </div>

</body>
</html>
