<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BituminImportBillDetails extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function bituminImportBill()
    {
        return $this->belongsTo(BituminImportBill::class);
    }


}
