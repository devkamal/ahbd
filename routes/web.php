<?php

use App\Models\User;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProfileController;
use App\Http\Middleware\RedirectIfAuthenticated;
use App\Http\Controllers\Frontend\UserController;
use App\Http\Controllers\Frontend\LoginController;
use App\Http\Controllers\Backend\ExpenseController;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\ExporterController;
use App\Http\Controllers\Backend\ImporterController;
use App\Http\Controllers\Backend\ExportbillController;
use App\Http\Controllers\Backend\ImportbillController;
use App\Http\Controllers\Backend\ExportForwardingBillController;
use App\Http\Controllers\Backend\ImportForwardingBillController;
use App\Http\Controllers\Backend\BituminForwardingBillController;
use App\Http\Controllers\Backend\BenapoleForwardingBillController;
use App\Http\Controllers\Backend\BituminImportBillController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


require __DIR__.'/auth.php';

//Admin middleware
Route::middleware(['auth', 'role:admin'])->group(function () {
    //Admin Dashboard
    Route::controller(AdminController::class)->group(function () {
        Route::get('/admin/dashboard', 'AdminDashboard')->name('admin.dashboard');
        Route::get('/logout', 'AdminDestroy')->name('admin.logout');
        Route::get('/admin/profile','AdminProfile')->name('admin_profile');
        Route::post('/admin/store','AdminStore')->name('admin_store');
        Route::get('/admin/password','AdminPassword')->name('password_change');
        Route::post('/update/password','updatePassword')->name('update_password');
    });


    //Exporter routing
    Route::controller(ExporterController::class)->prefix('exporter')->group(function(){
        Route::get('/view','view')->name('view_exporter');
        Route::post('/store','store')->name('store_exporter');
        Route::post('/update/{id}','update')->name('update_exporter');
        Route::get('/delete/{id}','delete')->name('delete_exporter');
        //get address name ways with json
        Route::get('/get-address/{id}','GetAddress');
    });

    //Importer routing
    Route::controller(ImporterController::class)->prefix('importer')->group(function(){
        Route::get('/view','view')->name('view_importer');
        Route::post('/store','store')->name('store_importer');
        Route::post('/update/{id}','update')->name('update_importer');
        Route::get('/delete/{id}','delete')->name('delete_importer');     
        //get address name ways with json
        Route::get('/get-address/{id}','GetAddress');
    });


    //Category routing
    Route::controller(CategoryController::class)->prefix('category')->group(function(){
        Route::get('/view','view')->name('view_category');
        Route::post('/store','store')->name('store_category');
        Route::post('/update/{id}','update')->name('update_category');
        Route::get('/delete/{id}','delete')->name('delete_category');
    });

    //Category routing
    Route::controller(ExpenseController::class)->prefix('expense')->group(function(){
        Route::get('/view','view')->name('view_expense');
        Route::post('/store','store')->name('store_expense');
        Route::get('/update/{id}','edit')->name('edit_expense');
        Route::post('/update/{id}','update')->name('update_expense');
        Route::get('/delete/{id}','delete')->name('delete_expense');
        //get category ways expense name
        // Route::get('/get-expense-name/{id}','GetExpenseName');
    });

    Route::get('/get-expense-details/{categoryId}', [ExpenseController::class, 'getExpenseDetails']);


    //Exportbill routing
    Route::controller(ExportbillController::class)->prefix('exportbill')->group(function () {
        Route::get('/view', 'view')->name('view_export_bill');
        Route::get('/add', 'add')->name('add_export_bill');
        Route::post('/store', 'store')->name('store_export_bill');
        Route::get('/edit/{id}', 'edit')->name('edit_export_bill');
        Route::get('/details/{id}', 'detailsView')->name('details_export_bill');
        Route::get('/pdf/{id}', 'pdfExportBill')->name('pdf_export_bill');

        Route::post('/update/{id}', 'update')->name('update_export_bill');
        Route::get('/delete/{id}', 'delete')->name('delete_export_bill');
    });
    /*************End Export*******************/

    //Importbill routing
    Route::controller(ImportbillController::class)->prefix('importbill')->group(function () {
        Route::get('/view', 'view')->name('view_import_bill');
        Route::get('/add', 'add')->name('add_import_bill');
        Route::post('/store', 'store')->name('store_import_bill');
        Route::get('/edit/{id}', 'edit')->name('edit_import_bill');
        Route::get('/details/{id}', 'detailsView')->name('details_import_bill');
        Route::get('/pdf/{id}', 'pdfImportBill')->name('pdf_import_bill');

        Route::post('/update/{id}', 'update')->name('update_import_bill');
        Route::get('/delete/{id}', 'delete')->name('delete_import_bill');
    });
    /*************End Import*******************/

    //Bitumin ImportBill routing
    Route::controller(BituminImportBillController::class)->prefix('bitumin-import-bill')->group(function () {
        Route::get('/view', 'view')->name('view_bitumin_import_bill');
        Route::get('/add', 'add')->name('add_bitumin_import_bill');
        Route::post('/store', 'store')->name('store_bitumin_import_bill');
        Route::get('/details/{id}', 'detailsView')->name('details_bitumin_import_bill');
        Route::get('/pdf/{id}', 'pdfBituminImportBill')->name('pdf_bitumin_import_bill');

        Route::get('/edit/{id}', 'edit')->name('edit_bitumin_import_bill');
        Route::post('/update/{id}', 'update')->name('update_bitumin_import_bill');

        Route::get('/delete/{id}', 'delete')->name('delete_bitumin_import_bill');
    });
    /*************End Bitumin Import Bill*******************/

    //ExportForwardingBill routing
    Route::controller(ExportForwardingBillController::class)->prefix('export-forwarding-bill')->group(function () {
        Route::get('/view', 'view')->name('view_export_forwarding_bill');
        Route::get('/add', 'add')->name('add_export_forwarding_bill');
        Route::post('/store', 'store')->name('store_export_forwarding_bill');
        Route::get('/edit/{id}', 'edit')->name('edit_export_forwarding_bill');
        Route::get('/details/{id}', 'detailsView')->name('details_export_forwarding_bill');
        Route::get('/pdf/{id}', 'pdfExportForwardingBill')->name('pdf_export_forwarding_bill');

        Route::post('/update/{id}', 'update')->name('update_export_forwarding_bill');
        Route::get('/delete/{id}', 'delete')->name('delete_export_forwarding_bill');

        //Get exporter name,address & billTo
        Route::get('/get-exporter/{id}','GetExporter');
        Route::get('/get-grand-total/{id}','GetGrandTotal');

    });
    /*************End ExportForwardingBill*******************/

    //ImportForwardingBill routing
    Route::controller(ImportForwardingBillController::class)->prefix('import-forwarding-bill')->group(function () {
        Route::get('/view', 'view')->name('view_import_forwarding_bill');
        Route::get('/add', 'add')->name('add_import_forwarding_bill');
        Route::post('/store', 'store')->name('store_import_forwarding_bill');
        Route::get('/edit/{id}', 'edit')->name('edit_import_forwarding_bill');
        Route::get('/details/{id}', 'detailsView')->name('details_import_forwarding_bill');
        Route::get('/pdf/{id}', 'pdfImportForwardingBill')->name('pdf_import_forwarding_bill');

        Route::post('/update/{id}', 'update')->name('update_import_forwarding_bill');
        Route::get('/delete/{id}', 'delete')->name('delete_import_forwarding_bill');

        //Get importer name,address & billTo
        Route::get('/get-importer/{id}','GetImporter');
        Route::get('/get-grand-total/{id}','GetGrandTotal');
    });
    /*************End ImportForwardingBill*******************/

    //Bitumin ForwardingBill routing
    Route::controller(BituminForwardingBillController::class)->prefix('bitumin-forwarding-bill')->group(function () {
        Route::get('/view', 'view')->name('view_bitumin_forwarding_bill');
        Route::get('/add', 'add')->name('add_bitumin_forwarding_bill');
        Route::post('/store', 'store')->name('store_bitumin_forwarding_bill');
        Route::get('/details/{id}', 'detailsView')->name('details_bitumin_forwarding_bill');
        Route::get('/pdf/{id}', 'pdfBituminForwardingBill')->name('pdf_bitumin_forwarding_bill');

        Route::get('/edit/{id}', 'edit')->name('edit_bitumin_forwarding_bill');
        Route::post('/update/{id}', 'update')->name('update_bitumin_forwarding_bill');

        Route::get('/delete/{id}', 'delete')->name('delete_bitumin_forwarding_bill');
    });
    /*************End Bitumin*******************/

    //benapole ForwardingBill routing
    Route::controller(BenapoleForwardingBillController::class)->prefix('benapole-forwarding-bill')->group(function () {
        Route::get('/view', 'view')->name('view_benapole_forwarding_bill');
        Route::get('/add', 'add')->name('add_benapole_forwarding_bill');
        Route::post('/store', 'store')->name('store_benapole_forwarding_bill');
        Route::get('/details/{id}', 'detailsView')->name('details_benapole_forwarding_bill');
        Route::get('/pdf/{id}', 'pdfBenapoleForwardingBill')->name('pdf_benapole_forwarding_bill');

        Route::get('/edit/{id}', 'edit')->name('edit_benapole_forwarding_bill');
        Route::post('/update/{id}', 'update')->name('update_benapole_forwarding_bill');

        Route::get('/delete/{id}', 'delete')->name('delete_benapole_forwarding_bill');
    });
    /*************End benapole*******************/

});  //End Admin middleware

Route::get('admin/login', [AdminController::class, 'AdminLogin'])->name('admin.login')->middleware(RedirectIfAuthenticated::class);
Route::get('/get/inactive/{id}', [ProductController::class, 'GetInActive']);
Route::get('/get-active/{id}', [ProductController::class, 'GetActive'])->name('get_active');


