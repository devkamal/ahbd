<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chowdhury Syndicate C&F Bill</title>
    <link rel="stylesheet" href="./style.css">

    <style>
      @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

      body {
         font-family: 'Poppins', sans-serif;
         margin: 20px;
      }

      h1 {
         color: #201D1D;
         font-size: 1.5rem;
         font-weight: 600;
      }

      .addresses {
         margin-top: 0.5rem;
      }

      .addresses p span {
         color: #201D1D;
         font-size: 0.75rem;
         font-weight: 600;
      }

      .addresses p {
         color: #474747;
         font-size: 0.75rem;
         font-weight: 400;
      }

      .addresses p:nth-child(1) {
         color: #201D1D;
         font-size: 0.75rem;
         font-weight: 500;
         text-transform: uppercase;
      }

      .bill {
         margin-top: 0;
         display: flex;
         align-items: center;
         justify-content: space-between;
      }

      .bill p {
         color: #474747;
         font-size: 0.75rem;
         font-weight: 400;
      }
      .bill p span {
         color: #201D1D;
         font-size: 0.75rem;
         font-weight: 600;
      }


      .dates {
         margin-top: 0.25rem;
      }

      .dates p {
         color: #201D1D;
         font-size: 0.75rem;
         font-weight: 400;
         line-height: 1.375rem;
      }

      .dates p span {
         color: #474747;
         font-size: 0.75rem;
         font-weight: 400;
      }

      .sub {
         margin-top: 1.19rem;
      }

      .sub p span {
         color: #474747;
         font-size: 0.75rem;
         font-weight: 400;
      }

      .sub p  {
         color: #201D1D;
         font-size: 0.75rem;
         font-weight: 600;
      }

      .paragraph {
         margin-top: 1.31rem;
      }

      .paragraph p {
         color: #474747;
         font-size: 0.75rem;
         font-weight: 400;
         line-height: 1.375rem;
      }

      .paragraph p span {
         color: #201D1D;
         font-size: 0.75rem;
         font-weight: 500;
         line-height: 1.375rem;
      }


      p {
         margin: 0;
      }

      .spacer {
         margin-bottom: 20px;
      }

      table {
         margin-top: 2.5rem;
         width: 100%;
         border-collapse: collapse;
         margin-top: 20px;
      }

      th, td {
         border: 1px solid #ddd;
         padding: 10px;
         text-align: left;
         font-weight: 400;
         font-size: 0.5rem;
      }

      th {
         background-color: #F3F3F3;
      }

      .grand-total {
         display: flex;
         justify-content: flex-end;
         margin-top: 10px;
         text-align: end;
         gap: 2rem;
      }

      .grand-total .price {
         font-weight: bold;
      }
      table td{
         font-weight: 600;
      }
      
      .closing {
         display: flex;
         justify-content: space-between;
         align-items: center;
         margin-top: 0.5rem;
      }

      .closing .vector {
         width: 16.4375rem;
         height: 3.8125rem;
         border: 1px solid #ECECEC;
         background: #F5F5F5;
      }

      table td{
         font-size:10px;
      }
      table th{
         font-size:10px;
      }

      @media print {
         body {
            margin: 1cm;
         }

         table {
            page-break-inside: avoid;
         }

         .closing-lines {
            page-break-before: always;
         }

         .hide-in-print {
            display: none;
         }
      }
    </style>
</head>
<body onload="window.print()">
    <h1>Chowdhury Syndicate</h1>

    <div class="addresses">
        <p>Shipping Clearing & Forwarding Agent</p>
        <p><span>Phone:</span> 031-2858488, 631613, 622405</p>
        <p><span>Location:</span> 950/B,AsadGonj,Chittagong, Phone:031-2858488,631613,622405 </p>
    </div>

    <div class="bill" style="margin-top:5px">
        <p><span>Bill No: </span>{{ $pdfbill->bill_no }}</p>
        <p><span>Sub: </span>C & F Bill</p>
        <p>Date: <span>{{ \Carbon\Carbon::parse($pdfbill->date)->format('d-m-Y')}}</span></p>
    </div>

    <hr style="margin-top: 0.56rem; background: #ECECEC;">

    <div class="dates">
        <p>To</p>
        <p>{{ $pdfbill['importer']['importer_name'] }}</p>
        <p>{{ $pdfbill['importer']['importer_address'] }}</p>
    </div>

    <!-- 1st Table -->
    <table>
        <tr>
            <th>Invoice No :</th>
            <th style="color: #474747"> {{ $pdfbill->invoice_no }}</th>

            <th>LC No</th>
            <th style="color: #474747">{{ $pdfbill->lc_no }}</th>
        </tr>
        <tr>
            <td>Invoice Date</td>
            <td style="color: #474747">{{ \Carbon\Carbon::parse($pdfbill->invoice_date)->format('d-m-Y')}}</td>
         
            <td>LC-DATE</td>
            <td style="color: #474747">{{ \Carbon\Carbon::parse($pdfbill->lc_date)->format('d-m-Y')}}</td>
        </tr>
        <tr>
            <td>Invoice Value (BDT)</td>
            <td>{{ $pdfbill->invoice_value }}</td>

            <td>BE-NO</td>
            <td style="color: #474747">{{ $pdfbill->be_no }}</td>
        </tr>
        <tr>
        <td>BDT Rate</td>  
        <td>{{ accounting_format($pdfbill->bdt_rate) }}</td>

            <td>BE-DATE</td>
            <td style="color: #474747">{{ \Carbon\Carbon::parse($pdfbill->be_date)->format('d-m-Y')}}</td>
        </tr>
        <tr>
            <td>Invoice Value (USD)</td>
            <td style="color: #474747">{{ $pdfbill->usd_value }}</td>
            <td>Consignment Of</td>
            <td style="color: #474747">{{ $pdfbill->consignment_of }}</td>
        </tr>
       
        <tr>
            <td>Rot No</td>
            <td style="color: #474747">{{ $pdfbill->rot_no }}</td>
            <td>Import SS</td>
            <td>{{ $pdfbill->import_ss }}</td>
        </tr>

        <tr>
            <td>IP No</td>
            <td style="color: #474747">{{ $pdfbill->ip_no }}</td>
            <td>IP Date</td>
            <td> {{ \Carbon\Carbon::parse($pdfbill->ip_date)->format('d-m-Y')}}</td>
        </tr>
   
    </table>
    
    <!-- 2nd Table -->
    <table>
        <tr>
            <th>Particular of Expenses</th>
            <th>Description</th>
            <th>Unit</th>
            <th>Rate</th>
            <th>QTY</th>
            <th>Amount</th>
        </tr>
        @foreach ($pdfbill->ImportExpenseDetails as $expense)
            <tr>
                  <td style="text-align:left">{{ $expense->expense_name }}</td>
                  <td>{{ $expense->description }}</td>
                  <td>{{ $expense->unit }}</td>
                  <td>{{ $expense->rate }}</td>
                  <td>{{ $expense->qty }}</td>
                  <td>{{ accounting_format($expense->amount) }}</td>
            </tr>
      @endforeach
    </table>


    <div class="closing">
        <div class="vector">
        {!! $pdfbill->notes !!}
        </div>
        <p style="font-size:10px;margin-top:40px">Amount in Words : {{numberToWord($pdfbill->grand_total)}}</p>
        <div class="grand-total">
            <div style="font-size: 0.625rem;">
                <p>Sub Total</p>
                <p>Agency Commission: ({{ $pdfbill->agency_percent }}%)</p>
                <p> Total Amount:</p>
            </div>
            <div style="font-size: 0.625rem;">
                <p class="price">{{ accounting_format($pdfbill->sub_total) }}</p>
                <p class="price">  
                     @if(isset($pdfbill->agency_fixed))
                        <tr>
                           <td colspan="5" style="text-align:right"><b>Agency Commission(Fixed):</b></td>
                           <td><b>{{ $pdfbill->agency_fixed }} </b></td>
                        </tr>
                     @elseif(isset($pdfbill->agency_percent))
                        <tr>
                           <td><b>{{ accounting_format($pdfbill->agency_value) }} </b></td>
                        </tr>
                     @else
                        <tr>
                           <td colspan="5" style="text-align:right"><b>Agency Commission:</b></td>
                           <td><b>No any Commission</b></td>
                        </tr>
                     @endif
                  </p>
                <p class="price">{{ accounting_format($pdfbill->grand_total) }}</p>
            </div>
        </div>
    </div>

</body>
</html>
