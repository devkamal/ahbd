@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <div class="row">
         <div class="col-xl-12 mx-auto">
           <div class="d-flex justify-content-between">
                <h6 class="mb-0 text-uppercase">Details import Bill</h6>
                <h6><a href="{{ route('view_import_bill') }}">Back</a></h6>
           </div>
            <hr/>
            <div class="card">
               <div class="card-body">
                  <div class="p-4 border rounded">
                     <form action="{{ route('update_import_bill',$details->id) }}" class="row g-3 needs-validation" method="post" novalidate>
                        @csrf
                        <div class="col-md-3">
                           <label for="date" class="form-label">Date <span class="text-danger">*</span></label>
                           <input type="date" class="form-control" readonly value="{{ $details->date }}" >
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                        <div class="col-md-3">
                           <label for="importer_id" class="form-label">importer name <span class="text-danger">*</span></label>
                           <select onchange="getAddress()" class="form-select" readonly  class="form-control">
                              @foreach ($importers as $item)
                                 <option value="{{ $item->id }}" {{ $item->id == $details->importer_id ? 'selected' : ''}}>{{ $item->importer_name }}</option>
                              @endforeach
                           </select>
                        </div>
                        <div class="col-md-3">
                           <label for="address" class="form-label">importer Address <span class="text-danger">*</span></label>
                           <input type="text" class="form-control" readonly value="{{ $details->importer_address }}" >
                        </div>

                        <div class="col-md-3">
                           <label for="consignment_of" class="form-label">Consignment Of </label>
                           <input type="text" class="form-control" readonly value="{{ $details->consignment_of }}" >
                        </div>
                        
                        <div class="col-md-3">
                           <label for="ip_no" class="form-label">IP No</label>
                           <input type="text" class="form-control" readonly value="{{ $details->ip_no }}" >
                        </div>

                        <div class="col-md-3">
                           <label for="ip_date" class="form-label">IP Date </label>
                           <input type="date" class="form-control" name="ip_date" id="ip_date" readonly value="{{ $details->ip_date }}" >
                        </div>
                 
                        <div class="col-md-3">
                           <label for="invoice_date" class="form-label">Invoice Date </label>
                           <input type="date" class="form-control" readonly value="{{ $details->invoice_date }}" >
                        </div>
                        <div class="col-md-3">
                           <label for="invoice_no" class="form-label">Invoice No</label>
                           <input type="text" class="form-control" readonly value="{{ $details->invoice_no }}" >
                        </div>
             
                        <hr>
                 
               
                        <div class="col-md-3">
                           <label for="be_date" class="form-label">B/E Date </label>
                           <input type="date" class="form-control" readonly value="{{ $details->be_date }}" >
                        </div>
                        <div class="col-md-3">
                           <label for="be_no" class="form-label">B/E No</label>
                           <input type="text" class="form-control" readonly value="{{ $details->be_no }}" >
                        </div>
                        <div class="col-md-3">
                           <label for="lc_date" class="form-label">L/C Date </label>
                           <input type="date" class="form-control" readonly value="{{ $details->lc_date }}" >
                        </div>
                        <div class="col-md-3">
                           <label for="lc_no" class="form-label">L/C No</label>
                           <input type="text" class="form-control" readonly value="{{ $details->lc_no }}" >
                        </div>

                        <div class="col-md-3">
                           <label for="rot_no" class="form-label">ROT No</label>
                           <input type="text" class="form-control" readonly value="{{ $details->rot_no }}" >
                        </div>
                        <div class="col-md-3">
                           <label for="import_ss" class="form-label">EX. S.S </label>
                           <input type="text" class="form-control" readonly value="{{ $details->import_ss }}" >
                        </div>


                        {{--<div class="col-md-3">
                           <label for="bl_no" class="form-label">B/L No <span class="text-danger">*</span></label>
                           <input type="text" class="form-control" readonly value="{{ $details->bl_no }}" >
                        </div>
                        <div class="col-md-3">
                           <label for="bl_date" class="form-label">B/L Date<span class="text-danger">*</span></label>
                           <input type="date" class="form-control" readonly value="{{ $details->bl_date }}" >
                        </div>--}}

                        <div class="col-md-3">
                            <label for="category_id" class="form-label">Bill Type <span class="text-danger">*</span></label>
                            <select class="form-select" readonly  class="form-control" onchange="getCategory()">
                                @foreach ($categories as $item)
                                    <option value="{{ $item->id }}" {{ $item->id == $details->category_id ? 'selected' : ''}}>{{ $item->category_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-3">
                            <label for="bill_no" class="form-label">Bill No <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" readonly value="{{ $details->bill_no }}" >
                        </div>


                        <div class="col-md-3">
                            <label for="usdValue" class="form-label">USD Value <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" readonly value="{{ $details->usd_value }}">
                            <div class="valid-feedback">Looks good!</div>
                        </div>
                        <div class="col-md-3">
                            <label for="bdtRate" class="form-label">BDT Rate <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" readonly value="{{ $details->bdt_rate }}">
                            <div class="valid-feedback">Looks good!</div>
                        </div>
                        <div class="col-md-3">
                            <label for="invoice_value" class="form-label">Invoice Value (BDT) </label>
                            <input type="text" class="form-control" readonly value="{{ $details->invoice_value }}">
                        </div>

                        <div class="col-md-12">
                            <div class="mt-4">
                            <label for="invoice_value" class="form-label">Notes:</label>
                            <textarea name="notes" id="notes" class="form-control" readonly>
                              {!! $details->notes !!}
                            </textarea>
                            </div>
                        </div>
                    
                        <hr>
                    

                          <!-------------Start calculation table------------------->     
                            <table class="table table-bordered" id="dynamic-table">
                                <thead class="bg-dark">
                                    <tr class="text-white text-center">
                                        <th scope="col">Particular of Expenses</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Unit</th>
                                        <th scope="col">Rate</th>
                                        <th scope="col">QTY</th>
                                        <th scope="col">Amount</th>
                                    </tr>
                                </thead>
                                @foreach ($details->ImportExpenseDetails as $expense)
                                    <tr>
                                          <td>{{ $expense->expense_name }}</td>
                                          <td>{{ $expense->description }}</td>
                                          <td>{{ $expense->unit }}</td>
                                          <td>{{ $expense->rate }}</td>
                                          <td>{{ $expense->qty }}</td>
                                          <td>{{ accounting_format($expense->amount) }}</td>
                                    </tr>
                              @endforeach
                              <tbody>
                                 <tr>
                                    <td colspan="5" style="text-align:right"><b>Sub Total:</b></td>
                                    <td><b>{{ accounting_format($details->sub_total) }}</b></td>
                                 </tr>
                                 @if(isset($details->agency_fixed))
                                       <tr>
                                          <td colspan="5" style="text-align:right"><b>Agency Commission(Fixed):</b></td>
                                          <td><b>{{ $details->agency_fixed }} </b></td>
                                       </tr>
                                    @elseif(isset($details->agency_percent))
                                       <tr>
                                          <td colspan="5" style="text-align:right"><b>Agency Commission: ({{ $details->agency_percent }}%)</b></td>
                                          <td><b>{{ $details->agency_value}} </b></td>
                                       </tr>
                                    @else
                                       <tr>
                                          <td colspan="5" style="text-align:right"><b>Agency Commission:</b></td>
                                          <td><b>No any Commission</b></td>
                                       </tr>
                                    @endif
                                 <tr>
                                    <td colspan="5" style="text-align:right"><b> Total Amount:</b></td>
                                    <td><b>{{ accounting_format($details->grand_total) }}</b></td>
                                 </tr>
                              </tbody>
                            </table>                       
                            <!-------------End calculation table------------------->                                  
                                                   
                            </div>
                         
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection