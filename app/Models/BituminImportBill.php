<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BituminImportBill extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function importer()
    {
        return $this->belongsTo(Importer::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function BituminImportBillDetails()
    {
        return $this->hasMany(BituminImportBillDetails::class);
    }

    public function priceNotes()
    {
        return $this->hasMany(PriceNote::class);
    }

}
