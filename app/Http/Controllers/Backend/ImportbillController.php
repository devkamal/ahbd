<?php

namespace App\Http\Controllers\Backend;

use App\Models\Category;
use App\Models\Importer;
use App\Models\ImportBill;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ImportExpenseDetails;

class ImportbillController extends Controller
{
    public function view(){
        $allData = ImportBill::orderBy('id','desc')->paginate(10);
        return view('backend.importbill.view_importbill',compact('allData'));
       }
    
       public function add(){
        $categories = Category::all();
        $importers = Importer::all();
        return view('backend.importbill.add_importbill',compact('importers','categories'));
       }
       

       public function store(Request $request)
       {
            //dd($request->all());

            $validateData = $request->validate([
                'date' => 'required',
                'importer_id' => 'required',
                'category_id' => 'required',
            ]);

            $importBill = ImportBill::create([
               'date' => $request->date,
               'importer_id' => $request->importer_id,
               'importer_address' => $request->importer_address,
               'invoice_date' => $request->invoice_date,
               'invoice_no' => $request->invoice_no,
               'consignment_of' => $request->consignment_of,
               'be_date' => $request->be_date,
               'be_no' => $request->be_no,
               'lc_date' => $request->lc_date,
               'lc_no' => $request->lc_no,
               'rot_no' => $request->rot_no,
               'import_ss' => $request->import_ss,
               'bl_no' => $request->bl_no,
               'bl_date' => $request->bl_date,
               'category_id' => $request->category_id,
               'bill_no' => $request->bill_no,
               'ip_no' => $request->ip_no,
               'ip_date' => $request->ip_date,
               'notes' => $request->notes,

               'sub_total' => $request->sub_total,
               'grand_total' => $request->grand_total,

               'usd_value' => $request->usd_value,
               'bdt_rate' => $request->bdt_rate,
               'invoice_value' => $request->invoice_value,
               
               'agency_fixed' => $request->agency_fixed,
               'agency_value' => $request->agency_value,
               'agency_percent' => $request->agency_percent,
           ]);
   
           // Create ExpenseDetails
           $details = [];
           foreach ($request->expense_name as $key => $expense_name) {
               $details[] = new ImportExpenseDetails([
                   'expense_name' => $expense_name,
                   'description' => $request->description[$key],
                //    'unit' => $request->unit[$key],
                   'rate' => $request->rate[$key],
                   'qty' => $request->qty[$key],
                   'amount' => $request->amount[$key],
               ]);
           }

   
           // Associate ImportExpenseDetails with ImportBill
           $importBill->ImportExpenseDetails()->saveMany($details);
   
          
         $notification = array(
            'message' => 'ImportBill Inserted Successfully',
               'alert-type' => 'success'
         );
   
         return redirect()->route('view_import_bill')->with($notification); 
       }
    
       public function edit($id){
         $editData = ImportBill::with('ImportExpenseDetails')->findOrFail($id);
         $importers = Importer::all();
         $categories = Category::all();
         return view('backend.importbill.edit_importbill',compact('editData','importers','categories'));
       }

       public function detailsView($id){
        $details = ImportBill::with('ImportExpenseDetails')->findOrFail($id);
        $importers = Importer::all();
        $categories = Category::all();
        return view('backend.importbill.details_importbill',compact('details','importers','categories'));
       }

       public function pdfImportBill($id){
        $pdfbill = ImportBill::with('ImportExpenseDetails')->findOrFail($id);
        $importers = Importer::all();
        $categories = Category::all();
        return view('backend.importbill.pdf_importbill',compact('pdfbill','importers','categories'));
       }
       
    
    public function update(Request $request, $id)
       {
       // dd($request->all());

           $exportBill = ImportBill::findOrFail($id);
       
           $exportBill->update([
            'date' => $request->date,
            'importer_id' => $request->importer_id,
            'importer_address' => $request->importer_address,
            'invoice_date' => $request->invoice_date,
            'invoice_no' => $request->invoice_no,
            'consignment_of' => $request->consignment_of,
            'be_date' => $request->be_date,
            'be_no' => $request->be_no,
            'lc_date' => $request->lc_date,
            'lc_no' => $request->lc_no,
            'rot_no' => $request->rot_no,
            'import_ss' => $request->import_ss,
            'bl_no' => $request->bl_no,
            'bl_date' => $request->bl_date,
            'category_id' => $request->category_id,
            'bill_no' => $request->bill_no,
            'ip_no' => $request->ip_no,
            'ip_date' => $request->ip_date,
            'notes' => $request->notes,

            'sub_total' => $request->sub_total,
            'grand_total' => $request->grand_total,

            'usd_value' => $request->usd_value,
            'bdt_rate' => $request->bdt_rate,
            'invoice_value' => $request->invoice_value,
            
            'agency_fixed' => $request->agency_fixed,
            'agency_value' => $request->agency_value,
            'agency_percent' => $request->agency_percent,
           ]);
       
           // Update or create ExpenseDetails
           $ImportExpenseDetails = [];
           foreach ($request->description as $key => $description) {
               $ImportExpenseDetails[] = [
                   'expense_name' => $request->expense_name[$key],
                   'description' => $request->description[$key],
                   'unit' => $request->unit[$key],
                   'rate' => $request->rate[$key],
                   'qty' => $request->qty[$key],
                   'amount' => $request->amount[$key],
               ];
           }
       
           $exportBill->ImportExpenseDetails()->delete(); // Delete existing records
           $exportBill->ImportExpenseDetails()->createMany($ImportExpenseDetails);
       
           $notification = [
               'message' => 'ImportBill Updated Successfully',
               'alert-type' => 'success',
           ];
       
           return redirect()->route('view_import_bill')->with($notification);
    }
       
       
    
    public function delete($id){
       $product = ImportBill::findOrFail($id);
       ImportBill::findOrFail($id)->delete();
    
       $details = ImportExpenseDetails::where('import_bill_id',$id)->get();
       foreach($details as $item){
           ImportExpenseDetails::where('import_bill_id',$id)->delete();
       }
    
       $notification = array(
           'message' => 'Export Bill Deleted Successfully',
           'alert-type' => 'success'
       );
    
       return redirect()->back()->with($notification);
    }
}
