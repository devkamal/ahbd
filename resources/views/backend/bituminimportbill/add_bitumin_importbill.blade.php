@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <div class="row">
         <div class="col-xl-12 mx-auto">
            <h6 class="mb-0 text-uppercase">Add New bitumin import Bill</h6>
            <hr/>
            <div class="card">
               <div class="card-body">
                  <div class="p-4 border rounded">
                     <form action="{{ route('store_bitumin_import_bill') }}" class="row g-3 needs-validation" method="post" novalidate>
                        @csrf
                        <div class="col-md-3">
                           <label for="date" class="form-label">Date <span class="text-danger">*</span></label>
                           <input type="date" class="form-control" name="date" id="date" required>
                           @error('date')
                               <span class="text-danger">{{ $message }}</span>
                           @enderror
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                        <div class="col-md-3">
                           <label for="importer_id" class="form-label">Importer name <span class="text-danger">*</span></label>
                           <select onchange="getAddress()" class="form-select" name="importer_id" id="importer_id" required class="form-control">
                              <option selected disabled value="">Select Name</option>
                              @foreach ($importers as $item)
                                 <option value="{{ $item->id }}">{{ $item->importer_name }}</option>
                              @endforeach
                           </select>
                           @error('importer_id')
                           <span class="text-danger">{{ $message }}</span>
                           @enderror
                        </div>
                        <div class="col-md-6">
                           <label for="address" class="form-label">Importer Address <span class="text-danger">*</span></label>
                           <input type="text" class="form-control" name="importer_address" id="importer_address" required>
                           <div class="valid-feedback">Looks good!</div>
                        </div>
               
                 
                        <div class="col-md-3">
                           <label for="invoice_date" class="form-label">Invoice Date <span class="text-danger">*</span></label>
                           <input type="date" class="form-control" name="invoice_date" id="invoice_date" required>
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                        <div class="col-md-3">
                           <label for="invoice_no" class="form-label">Invoice No<span class="text-danger">*</span></label>
                           <input type="text" class="form-control" name="invoice_no" id="invoice_no" required>
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                        <div class="col-md-3">
                           <label for="consignment_of" class="form-label">Consignment Of <span class="text-danger">*</span></label>
                           <input type="text" class="form-control" name="consignment_of" id="consignment_of" required>
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                        <hr>
                 
               
                        <div class="col-md-3">
                           <label for="be_date" class="form-label">B/E Date <span class="text-danger">*</span></label>
                           <input type="date" class="form-control" name="be_date" id="be_date" required>
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                        <div class="col-md-3">
                           <label for="be_no" class="form-label">B/E No<span class="text-danger">*</span></label>
                           <input type="text" class="form-control" name="be_no" id="be_no" required>
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                        <div class="col-md-3">
                           <label for="lc_date" class="form-label">L/C Date <span class="text-danger">*</span></label>
                           <input type="date" class="form-control" name="lc_date" id="lc_date" required>
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                        <div class="col-md-3">
                           <label for="lc_no" class="form-label">L/C No<span class="text-danger">*</span></label>
                           <input type="text" class="form-control" name="lc_no" id="lc_no" required>
                           <div class="valid-feedback">Looks good!</div>
                        </div>

                        <div class="col-md-3">
                           <label for="rot_no" class="form-label">ROT No<span class="text-danger">*</span></label>
                           <input type="text" class="form-control" name="rot_no" id="rot_no" required>
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                        <div class="col-md-3">
                           <label for="import_ss" class="form-label">EX. S.S <span class="text-danger">*</span></label>
                           <input type="text" class="form-control" name="import_ss" id="import_ss" required>
                           <div class="valid-feedback">Looks good!</div>
                        </div>


                        <div class="col-md-3">
                            <label for="category_id" class="form-label">Bill Type <span class="text-danger">*</span></label>
                            <select class="form-select" name="category_id" id="category_id" required class="form-control" onchange="getCategory()">
                                <option value="">Select Bill</option>
                                @foreach ($categories as $item)
                                    <option value="{{ $item->id }}">{{ $item->category_name}}</option>
                                @endforeach
                            </select>
                            @error('category_id')
                           <span class="text-danger">{{ $message }}</span>
                           @enderror
                        </div>

                        <div class="col-md-3">
                            <label for="bill_no" class="form-label">Bill No <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="bill_no" id="bill_no" value="{{ bill_no() }}" required>
                            <div class="valid-feedback">Looks good!</div>
                        </div>

                        <div class="col-md-3">
                            <label for="usdValue" class="form-label">USD Value <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="usd_value" id="usdValue" required>
                            <div class="valid-feedback">Looks good!</div>
                        </div>
                        <div class="col-md-3">
                            <label for="bdtRate" class="form-label">BDT Rate <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="bdt_rate" id="bdtRate" required>
                            <div class="valid-feedback">Looks good!</div>
                        </div>
                        <div class="col-md-3">
                            <label for="invoice_value" class="form-label">Invoice Value (BDT) <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="invoice_value" id="invoice_value" required>
                            <div class="valid-feedback">Looks good!</div>
                        </div>
                    
                        <hr>
                    

                          <!-------------Start calculation table------------------->     
                          <table class="table table-bordered" id="dynamic-table">
                           <thead class="bg-dark">
                              <tr class="text-white text-center">
                                 <th scope="col">Particular of Expenses</th>
                                 <th scope="col">Description</th>
                                 {{--<th scope="col">Unit</th>--}}
                                 <th scope="col">Rate</th>
                                 <th scope="col">QTY</th>
                                 <th scope="col">Amount</th>
                                 <th scope="col" colspan="1" class="text-center">Action</th>
                              </tr>
                           </thead>
                           <tbody id="expense-details-body">
                           </tbody>
                        </table>
                        <!-------------End calculation table------------------->                                  
                        </div>
                        <!-- Start Table for Calculation -->
                        <div class="row">
                        <div class="col-lg-8">
                           <table class="mt-2">
                              <thead>
                                 <th>Description</th>
                                 <th>Amount</th>
                                 <th>Description</th>
                                 <th>Amount</th>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>
                                       <input type="text" name="notes[]" id="notes" class="form-control">
                                    </td>
                                    <td>
                                       <input type="text" name="price[]" id="price" class="form-control" oninput="calculateDueAmount()">
                                    </td>
                                    <td>
                                       <input type="text" name="notes[]" id="notes" class="form-control">
                                    </td>
                                    <td>
                                       <input type="text" name="price[]" id="price" class="form-control" oninput="calculateDueAmount()">
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <input type="text" name="notes[]" id="notes" class="form-control">
                                    </td>
                                    <td>
                                       <input type="text" name="price[]" id="price" class="form-control" oninput="calculateDueAmount()">
                                    </td>
                                    <td>
                                       <input type="text" name="notes[]" id="notes" class="form-control">
                                    </td>
                                    <td>
                                       <input type="text" name="price[]" id="price" class="form-control" oninput="calculateDueAmount()">
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <input type="text" name="notes[]" id="notes" class="form-control">
                                    </td>
                                    <td>
                                       <input type="text" name="price[]" id="price" class="form-control" oninput="calculateDueAmount()">
                                    </td>
                                    <td>
                                       <input type="text" name="notes[]" id="notes" class="form-control">
                                    </td>
                                    <td>
                                       <input type="text" name="price[]" id="price" class="form-control" oninput="calculateDueAmount()">
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <input type="text" name="notes[]" id="notes" class="form-control">
                                    </td>
                                    <td>
                                       <input type="text" name="price[]" id="price" class="form-control" oninput="calculateDueAmount()">
                                    </td>
                                    <td>
                                       <input type="text" name="notes[]" id="notes" class="form-control">
                                    </td>
                                    <td>
                                       <input type="text" name="price[]" id="price" class="form-control" oninput="calculateDueAmount()">
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                            
                        </div>

                        <div class="col-lg-4">
                        <table class="table table-bordered">
                        <tr>
                        <th>Sub Total:</th>
                        <td id="sub_total_display">TK: 0</td>
                        <input type="hidden" name="sub_total" id="sub_total" value="0">
                        </tr>
                        <tr>
                        <th>Agency Commission (fixed)</th>
                        <td>
                        <input type="text" name="agency_fixed" id="agency_fixed" class="form-control" oninput="calculateGrandTotal()">
                        </td>
                        <td></td>
                        </tr>

                        <tr>
                        <th>Agency Commission (%)</th>
                        <td class="d-flex">
                        <input type="text" name="agency_percent" id="agency_percent" class="form-control" placeholder="%" 
                        oninput="calculateAgencyValue()">

                        <input type="text" name="agency_value" id="agency_value" class="form-control">
                        </td>
                        </tr>

                        <tr>
                        <th> Total Amount:</th>
                        <td id="grandTotalDisplay">TK: 0</td>
                        <input type="hidden" name="grand_total" id="grand_total" value="0">
                        </tr>

                        <tr>
                        <th>Less Received :</th>
                        <td>
                        <input type="text" id="paid_amount" name="paid_amount" class="form-control" oninput="calculateDueAmount()">
                        </td>
                        </tr>

                        <tr>
                        <th>Net Balance:</th>
                        <td id="due_amountDisplay">TK: 0</td>
                        <input type="hidden" name="due_amount" id="due_amount">
                        </tr>

                        </table>
                        </div>
                        </div>
                        <!-- End Table for Calculation -->
                        

                        <div class="col-12">
                           <button class="btn btn-primary" type="submit">Submit form</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        // Add input event listeners
        document.getElementById('usdValue').addEventListener('input', updateInvoiceValue);
        document.getElementById('bdtRate').addEventListener('input', updateInvoiceValue);
        document.getElementById('agency_percent').addEventListener('input', calculateAgencyValue);

        // Initial calculation
        updateInvoiceValue();
        calculateAgencyValue();
    });

    function updateInvoiceValue() {
        // Get the input values
        var usdValue = parseFloat(document.getElementById('usdValue').value) || 0;
        var bdtRate = parseFloat(document.getElementById('bdtRate').value) || 0;

        // Calculate the invoice value
        var invoiceValue = usdValue * bdtRate;

        // Display the result
        document.getElementById('invoice_value').value = invoiceValue.toFixed(2);

        // Trigger the agency value calculation when invoice value changes
        calculateAgencyValue();
    }

    function calculateAgencyValue() {
        // Get the input values
        var invoiceValue = parseFloat(document.getElementById('invoice_value').value) || 0;
        var agencyPercent = parseFloat(document.getElementById('agency_percent').value) || 0;

        // Calculate the agency value
        var agencyValue = (invoiceValue !== 0) ? (agencyPercent / 100) * invoiceValue : 0;

        // Display the result
        document.getElementById('agency_value').value = agencyValue.toFixed();

        // Trigger the grand total calculation when agency value changes
        calculateGrandTotal();
    }

    function calculateGrandTotal() {
        var subTotal = 0;
        var amountInputs = document.getElementsByName('amount[]');

        amountInputs.forEach(function (input) {
            subTotal += parseFloat(input.value) || 0;
        });

        var agencyFixed = parseFloat(document.getElementById('agency_fixed').value) || 0;
        var agencyValue = parseFloat(document.getElementById('agency_value').value) || 0;

        // Calculate the grand total including subTotal, agency_fixed, and agency_value
        var grandTotal = subTotal + agencyFixed + agencyValue;

        document.getElementById('sub_total_display').textContent = 'TK: ' + subTotal.toFixed(2);
        document.getElementById('sub_total').value = subTotal.toFixed(2);

        document.getElementById('grandTotalDisplay').textContent = 'TK: ' + grandTotal.toFixed(2);
        document.getElementById('grand_total').value = grandTotal.toFixed(2);

        calculateDueAmount();
    }

    document.addEventListener('input', function (event) {
      if (event.target && event.target.name === 'amount[]') {
         calculateGrandTotal();
      }
   });

</script>


<script>
   function getCategory() {
       var categoryId = document.getElementById('category_id').value;
   
       // Make an AJAX request to fetch data based on the selected category
       fetch('/get-expense-details/' + categoryId)
           .then(response => response.json())
           .then(data => {
               // Populate the table with the fetched data
               populateTable(data);
               // Calculate and update grand total
               calculateGrandTotal();
           })
           .catch(error => console.error('Error:', error));
   }
   
   function populateTable(data) {
       var tbody = document.getElementById('expense-details-body');
       tbody.innerHTML = ''; // Clear previous content
   
       // Iterate over the data and create table rows
       data.forEach(row => {
           var tr = document.createElement('tr');
   
           // Create and append table cells with input fields
            var td1 = document.createElement('td');
            var inputExpenseName = createInput('text', 'expense_name[]', 'form-control');
            inputExpenseName.value = row.expense_name; // Set the value from the data
            td1.appendChild(inputExpenseName);
            tr.appendChild(td1);
   
           var td2 = document.createElement('td');
           var inputDesc = createInput('text', 'description[]', 'form-control');
           td2.appendChild(inputDesc);
           tr.appendChild(td2);
   
         //   var td3 = document.createElement('td');
         //   var inputUnit = createInput('text', 'unit[]', 'form-control');
         //   td3.appendChild(inputUnit);
         //   tr.appendChild(td3);
   
           var td4 = document.createElement('td');
           var inputRate = createInput('text', 'rate[]', 'form-control');
           inputRate.addEventListener('input', updateAmount);
           td4.appendChild(inputRate);
           tr.appendChild(td4);
   
           var td5 = document.createElement('td');
           var inputQty = createInput('text', 'qty[]', 'form-control');
           inputQty.addEventListener('input', updateAmount);
           td5.appendChild(inputQty);
           tr.appendChild(td5);
   
           var td6 = document.createElement('td');
           var inputAmount = createInput('text', 'amount[]', 'form-control');
           td6.appendChild(inputAmount);
           tr.appendChild(td6);
   
           var td7 = document.createElement('td');
           var minusButton = createButton('button', 'btn btn-danger', 'X', removeRow);
           td7.appendChild(minusButton);
           tr.appendChild(td7);
   
           tbody.appendChild(tr);
       });
   }
   
   function createInput(type, name, className) {
       var input = document.createElement('input');
       input.type = type;
       input.name = name;
       input.className = className;
       return input;
   }
   
   function createButton(type, className, text, clickFunction) {
       var button = document.createElement('button');
       button.type = type;
       button.className = className;
       button.textContent = text;
       button.addEventListener('click', clickFunction);
       return button;
   }
   
   function removeRow() {
   var row = this.parentNode.parentNode;
   row.parentNode.removeChild(row);
   // Recalculate grand total when a row is removed
   calculateGrandTotal();
   }
   
   function updateAmount() {
   var row = this.parentNode.parentNode;
   var rate = parseFloat(row.querySelector('[name="rate[]"]').value) || 0;
   var qty = parseFloat(row.querySelector('[name="qty[]"]').value) || 0;
   var amountField = row.querySelector('[name="amount[]"]');
   var amount = rate * qty;
   
   // Set the value of the amount field
   amountField.value = amount.toFixed(2);
   
   // Recalculate grand total when amount is updated
   calculateGrandTotal();
   }
   
   
//    function calculateDueAmount() {
//        var subTotal = parseFloat(document.getElementById('sub_total_display').textContent.replace('TK: ', '')) || 0;
//        var agencyFixed = parseFloat(document.getElementById('agency_fixed').value) || 0;
   
//        // Calculate the grand total including agency_fixed and subTotal
//        var grandTotal = subTotal + agencyFixed;
   
//        document.getElementById('sub_total_display').textContent = 'TK: ' + subTotal.toFixed(2);
//        document.getElementById('sub_total').value = subTotal.toFixed(2);
   
//        document.getElementById('grandTotalDisplay').textContent = 'TK: ' + grandTotal.toFixed(2);
//        document.getElementById('grand_total').value = grandTotal.toFixed(2);
   
//        var paidAmount = parseFloat(document.getElementById('paid_amount').value) || 0;
//        var dueAmount = grandTotal - paidAmount;
   
//        document.getElementById('due_amountDisplay').textContent = 'TK: ' + dueAmount.toFixed(2);
//        document.getElementById('due_amount').value = dueAmount.toFixed(2);
//    }


function calculateDueAmount() {
    // Calculate subTotal and agencyFixed
    var subTotal = parseFloat(document.getElementById('sub_total_display').textContent.replace('TK: ', '')) || 0;
    var agencyFixed = parseFloat(document.getElementById('agency_fixed').value) || 0;

    // Calculate the grand total including agency_fixed and subTotal
    var grandTotal = subTotal + agencyFixed;

    // Display subTotal and grandTotal
    document.getElementById('sub_total_display').textContent = 'TK: ' + subTotal.toFixed(2);
    document.getElementById('sub_total').value = subTotal.toFixed(2);

    document.getElementById('grandTotalDisplay').textContent = 'TK: ' + grandTotal.toFixed(2);
    document.getElementById('grand_total').value = grandTotal.toFixed(2);

    // Calculate total price from "price" inputs
    var totalPrice = 0;

    // Iterate through all "price" inputs and add their values
    var priceInputs = document.getElementsByName('price[]');
    for (var i = 0; i < priceInputs.length; i++) {
        totalPrice += parseFloat(priceInputs[i].value) || 0;
    }

    // Update dueAmount and display
    var paidAmount = totalPrice; // Set paidAmount to totalPrice
    var dueAmount = grandTotal - paidAmount;

    document.getElementById('due_amountDisplay').textContent = 'TK: ' + dueAmount.toFixed(2);
    document.getElementById('due_amount').value = dueAmount.toFixed(2);

    // Display the total price as the paid_amount
    document.getElementById('paid_amount').value = paidAmount.toFixed(2);
   
}

   
   
</script>


<!-- // Bill Type ways Bill No -->
<script>
    var billNoMap = {};

    document.getElementById('category_id').addEventListener('change', function () {
        var selectedOption = this.options[this.selectedIndex];
        var categoryName = selectedOption.text.trim().toUpperCase();
        var billNoInput = document.getElementById('bill_no');

        // Check if the bill number is already generated for the selected category
        if (categoryName in billNoMap) {
            billNoInput.value = billNoMap[categoryName];
        } else {
            var dynamicBillNo = generateDynamicBillNo();
            var currentYear = new Date().getFullYear().toString().slice(-2);
            var startBillNo = 10001;

            var last7DigitsFromCategory = categoryName.slice(-9);
            billNoMap[categoryName] = last7DigitsFromCategory + '/' + currentYear + '-' + (startBillNo + dynamicBillNo);
            billNoInput.value = billNoMap[categoryName];
        }
    });

    function generateDynamicBillNo() {
        return Math.floor(Math.random() * 1000);
    }
</script>



<script>
   //Get expoert ways address
   function getAddress() {
        console.log('Function getAddress called');
        var id = $('#importer_id').val();
        $.ajax({
            url: '/importer/get-address/' + id,
            type: 'GET'
        })
        .done(function(response) {
            console.log('Success. Response:', response);
            $('#importer_address').val(response.importer_address);
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            console.error('Error fetching supplier mobile:', textStatus, errorThrown);
        });
    }

</script>



@endsection