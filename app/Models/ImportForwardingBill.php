<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImportForwardingBill extends Model
{
    use HasFactory;
    protected $guarded = [];


    public function importer()
    {
        return $this->belongsTo(Importer::class);
    }

    public function details()
    {
        return $this->hasMany(ImportForwardingBillDetail::class);
    }
}
