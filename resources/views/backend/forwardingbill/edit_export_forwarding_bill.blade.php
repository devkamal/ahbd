@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <div class="row">
         <div class="col-lg-12">
            <div class="card mb-4">
               <div class="card text-left">
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                    <h4 class="card-title mb-3">Update Export Forwarding Bill</h4>
                     <h4 class="card-title mb-3">
                        <a href="{{ route('view_export_forwarding_bill') }}">Back</a>
                     </h4>
                    </div>
                     <form action="{{ route('update_export_forwarding_bill',$editData->id) }}" method="POST">
                        @csrf
                        <div class="row">
                           <div class="col-lg-4">
                              <table class="table table-bordered">
                                 <tbody>
                                    <tr>
                                       <td colspan="2">
                                          <select name="exporter_id" id="exporter_id" class="form-control" onchange="getExporter()">
                                             <option value="">Select Exporter</option>
                                             @foreach ($exporters as $item)
                                                <option value="{{ $item->id }}" {{ $item->id == $editData->exporter_id ? 'selected' : ''}}>{{ $item->exporter_name }}</option>
                                              @endforeach
                                          </select>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Bill To</td>
                                       <td>
                                          <input type="text" name="bill_to" id="bill_to" class="form-control" value="{{ $editData->bill_to }}">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Exporter Name</td>
                                       <td>
                                          <input type="text" name="exporter_name" id="exporter_name" class="form-control" value="{{ $editData->exporter_name }}">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Exporter Address</td>
                                       <td>
                                          <div class="form-group">
                                             <input type="text" class="form-control" name="exporter_address" id="exporter_address"  value="{{ $editData->exporter_address }}">
                                          </div>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                           <div class="col-lg-4"></div>
                           <div class="col-lg-4"></div>
                        </div>
                        <div>

                        <span>Sub: </span>
                        <input type="text" name="sub" class="form-control" id="sub" value="Submission of C&F Bill No:02"> <br>
                        <span>Message (Body): </span>
                        <input type="text" name="message" class="form-control" id="message" 
                        value="We are submitting here with the following C&F Bill and in this connection we would also request you to kindly
                        arrage payment of the same at an early date from your end"> <br>

                        

                        <!-------------Start calculation table------------------->     
                        <table class="table table-bordered" id="dynamic-table">
                              <thead class="bg-dark">
                                    <tr class="text-white text-center">
                                       <th scope="col">SL/No</th>
                                       <th scope="col">Bill No & Date</th>
                                       <th scope="col">Invoice No</th>
                                       <th scope="col">Invoice Date</th>
                                       <th scope="col">Bill Amount (TK)</th>
                                    </tr>
                              </thead>
                              @foreach ($editData->details as $key => $expense)
                                    <tr class="text-center">
                                    <td>{{ $key+1 }}</td>
                                    <td>Bill No:{{ $expense->bill_no }} | Date:{{ \Carbon\Carbon::parse($expense->created_at)->format('d-m-Y')}}</td>
                                    <td> {{ $expense->invoice_no }}</td>
                                    <td> {{ $expense->invoice_date }}</td>
                                    <td>{{ $expense->amount }}</td>
                                    </tr>
                              @endforeach
                              <tbody>
                                 <tr>
                                    <td style="text-align:right" colspan="4"><b> Total Amount:</b></td>
                                    <td class="text-center"><b>{{ $editData->grand_total }}</b></td>
                                 </tr>
                              </tbody>
                           </table>                     
                            <!-------------End calculation table------------------->  


                          
                           <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>



<script>
   //Get exporter ways billTo, name, address
   function getExporter() {
       var id = $('#exporter_id').val();
       $.ajax({
           url: '/export-forwarding-bill/get-exporter/' + id,
           type: 'GET'
       })
       .done(function(response) {
           $('#bill_to').val(response.bill_to);
           $('#exporter_name').val(response.exporter_name);
           $('#exporter_address').val(response.exporter_address);
       })
       .fail(function(jqXHR, textStatus, errorThrown) {
           console.error('Error fetching exporter details:', textStatus, errorThrown);
       });
   }
   

   //Get bill no ways get amount
   function getAmount(selectElement) {
       var id = $(selectElement).val();
       $.ajax({
         url: '/export-forwarding-bill/get-grand-total/' + id,
           type: 'GET'
       })
       .done(function(response) {
           var invoice_no = response.invoice_no;
           var invoice_date = response.invoice_date;
         //   var consignment_of = response.consignment_of;
           var amount = response.amount || 0;
           $(selectElement).closest('tr').find('input[name="invoice_no[]"]').val(invoice_no);
           $(selectElement).closest('tr').find('input[name="invoice_date[]"]').val(invoice_date);
         //   $(selectElement).closest('tr').find('input[name="consignment_of[]"]').val(consignment_of);
           $(selectElement).closest('tr').find('input[name="amount[]"]').val(amount);
           updateGrandTotal();
       })
       .fail(function(jqXHR, textStatus, errorThrown) {
           console.error('Error fetching grand total:', textStatus, errorThrown);
       });
   }
  
</script>


@endsection