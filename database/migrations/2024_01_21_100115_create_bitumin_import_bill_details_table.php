<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitumin_import_bill_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('bitumin_import_bill_id');
            $table->foreign('bitumin_import_bill_id')->references('id')->on('bitumin_import_bills')->onDelete('cascade');
            $table->string('expense_name')->nullable();
            $table->text('description')->nullable();
            $table->text('notes')->nullable();
            $table->string('price')->nullable();
            $table->string('unit')->nullable();
            $table->decimal('rate', 10, 2)->nullable();
            $table->decimal('qty', 10, 2)->nullable();
            $table->decimal('amount', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitumin_import_bill_details');
    }
};
