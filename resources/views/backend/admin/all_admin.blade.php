@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">Admin</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">All Admin</li>
               </ol>
            </nav>
         </div>
      
      </div>
    
      <hr>
      <div class="card">
         <div class="card-body">
            <div class="table-responsive">
               <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5">
                 
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="example" class="table table-striped table-bordered dataTable" style="width: 100%;" role="grid" aria-describedby="example_info">
                           <thead>
                              <tr role="row">
                                 <th>Sl</th>
                                 <th>Image </th>
                                 <th>Name </th>
                                 <th>Email </th>
                                 <th>Phone </th>
                                 <th>Role </th> 
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach ($allAdmin as $key => $item)
                                    <tr role="row" class="odd">
                                          <td class="sorting_1">{{ $key+1 }}</td>
                                          <td> <img src="{{ (!empty($item->image)) ? url('upload/adminimg/'.$item->image):url('upload/noimg.png') }}" style="width: 30px;">  </td>
                                          <td>{{ $item->name }}</td>
                                          <td>{{ $item->email }}</td>
                                          <td>{{ $item->phone }}</td>
                                          <td>
                                             @foreach ($item->roles as $role)
                                                <span class="text-danger">{{ $role->name }}</span>
                                             @endforeach
                                          </td> 
                                          <td>
                                                <a href="{{ route('rolewise.edit.admin',$item->id) }}" title="edit" class="btn btn-primary btn-sm">Eidt</a>
                                                <a href="{{ route('delete.admin',$item->id) }}" id="delete" title="delete" class="btn btn-danger btn-sm">Delete</a>
                                          </td>
                                    </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection