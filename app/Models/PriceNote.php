<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PriceNote extends Model
{
    use HasFactory;
    protected $guarded = [];

    // Inside NotesPrice model
    public function BituminImportBill()
    {
        return $this->belongsTo(BituminImportBill::class);
    }

}
