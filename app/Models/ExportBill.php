<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExportBill extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function exporter()
    {
        return $this->belongsTo(Exporter::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function expenseDetails()
    {
        return $this->hasMany(ExpenseDetails::class);
    }
    
}
