<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImportBill extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function importer()
    {
        return $this->belongsTo(Importer::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function ImportExpenseDetails()
    {
        return $this->hasMany(ImportExpenseDetails::class);
    }

}
