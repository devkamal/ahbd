@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <div class="row">
         <div class="col-xl-12 mx-auto">
           <div class="d-flex justify-content-between">
                <h6 class="mb-0 text-uppercase">Details Import Forwarding Bill</h6>
                <h6><a href="{{ route('view_import_forwarding_bill') }}">Back</a></h6>
           </div>
            <hr/>
            <div class="card">
               <div class="card-body">
                  <div class="p-4 border rounded">
                     <form action="{{ route('update_export_bill',$details->id) }}" class="row g-3 needs-validation" method="post" novalidate>
                        @csrf
                        <div class="col-md-4">
                           <label for="exporter_id" class="form-label">Importer name <span class="text-danger">*</span></label>
                           <select onchange="getAddress()" class="form-select" readonly required class="form-control">
                              @foreach ($importers as $item)
                                 <option value="{{ $item->id }}" {{ $item->id == $details->importer_id ? 'selected' : ''}}>{{ $item->importer_name }}</option>
                              @endforeach
                           </select>
                        </div>
                        <div class="col-md-4">
                           <label for="address" class="form-label">Importer Address <span class="text-danger">*</span></label>
                           <input type="text" class="form-control" readonly value="{{ $details->importer_address }}" required>
                        </div>
                        <div class="col-md-4">
                           <label for="ip_no" class="form-label">Bill To<span class="text-danger">*</span></label>
                           <input type="text" class="form-control" readonly value="{{ $details->bill_to }}" required>
                        </div>
                 
                        <div class="col-md-4">
                           <label for="sub" class="form-label">Subject <span class="text-danger">*</span></label>
                           <input type="text" class="form-control" readonly value="{{ $details->sub }}" required>
                        </div>

                        <div class="col-md-4">
                           <label for="message" class="form-label">Message<span class="text-danger">*</span></label>
                           <input type="text" class="form-control" readonly value="{{ $details->message }}" required>
                        </div>
                        <div class="col-md-3">
                           <label for="grand_total" class="form-label"> Total Amount<span class="text-danger">*</span></label>
                           <input type="text" class="form-control" readonly value="{{ $details->grand_total }}" required>
                        </div>
                        
                        <hr>
                    

                          <!-------------Start calculation table------------------->     
                            <table class="table table-bordered" id="dynamic-table">
                                <thead class="bg-dark">
                                    <tr class="text-white text-center">
                                        <th scope="col">Bill No</th>
                                        <th scope="col">BL No</th>
                                        <th scope="col">IP No</th>
                                        <th scope="col">Buyer</th>
                                        <th scope="col">Amount</th>
                                    </tr>
                                </thead>
                                @foreach ($details->details as $expense)
                                    <tr class="text-center">
                                          <td>{{ $expense->bill_no }}</td>
                                          <td>{{ $expense->bl_no }}</td>
                                          <td>{{ $expense->ip_no }}</td>
                                          <td>{{ $expense->buyer }}</td>
                                          <td>{{ $expense->amount }}</td>
                                    </tr>
                              @endforeach
                            </table>                       
                            <!-------------End calculation table------------------->                                  
                              <div>
                                   <div class="row">
                                   <div class="col-md-4"></div>
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                          <h3> Total Amount:{{ $details->grand_total }}</h3>
                                    </div>
                                   </div>
                              </div>        
                            </div>
                         
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection