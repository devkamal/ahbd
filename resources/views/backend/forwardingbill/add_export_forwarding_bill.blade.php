@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <div class="row">
         <div class="col-lg-12">
            <div class="card mb-4">
               <div class="card text-left">
                  <div class="card-body">
                     <h4 class="card-title mb-3">Add New Export Forwarding Bill</h4>
                     <form action="{{ route('store_export_forwarding_bill') }}" method="POST">
                        @csrf
                        <div class="row">
                           <div class="col-lg-4">
                              <table class="table table-bordered">
                                 <tbody>
                                    <tr>
                                       <td colspan="2">
                                          <select name="exporter_id" id="exporter_id" class="form-control" onchange="getExporter()">
                                             <option value="">Select Exporter</option>
                                             @foreach ($exporters as $item)
                                             <option value="{{ $item->id }}">{{ $item->exporter_name }}</option>
                                             @endforeach
                                          </select>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Bill To</td>
                                       <td>
                                          <input type="text" name="bill_to" id="bill_to" class="form-control">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Exporter Name</td>
                                       <td>
                                          <input type="text" name="exporter_name" id="exporter_name" class="form-control" placeholder="Exporter Name">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Exporter Address</td>
                                       <td>
                                          <div class="form-group">
                                             <input type="text" class="form-control" name="exporter_address" id="exporter_address" placeholder="address" pwa2-uuid="EDITOR/input-4F3-D08-7BBBF-5C2" pwa-fake-editor="">
                                          </div>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                           <div class="col-lg-4"></div>
                           <div class="col-lg-4"></div>
                        </div>
                        <div>

                        <span>Sub: </span>
                        <input type="text" name="sub" class="form-control" id="sub" value="Submission of C&F Bill No:02"> <br>
                        <span>Message (Body): </span>
                        <input type="text" name="message" class="form-control" id="message" 
                        value="We are submitting here with the following C&F Bill and in this connection we would also request you to kindly
                        arrage payment of the same at an early date from your end"> <br>

                        

                           <table class="table table-bordered">
                              <thead class="bg-dark">
                                 <tr class="text-white text-center">
                                    <th scope="col">Sl</th>
                                    <th scope="col">Bill No </th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Invoice No</th>
                                    <th scope="col">Invoice Date</th>
                                    {{--<th scope="col">Quantity</th>--}}
                                    {{--<th scope="col">Buyer</th>--}}
                                    <th scope="col">Amount</th>
                                    <th scope="col">Actions</th>
                                 </tr>
                              </thead>
                              <tbody id="table-body">
                                 <tr>
                                    <td class="text-center">1</td>
                                    <td>
                                       <select name="bill_no[]" id="bill_no" class="form-control select2" onchange="getAmount(this)">
                                          <option value=""></option>
                                          @foreach ($exportBills as $item)
                                          <option value="{{ $item->id }}">{{ $item->bill_no}}</option>
                                          @endforeach
                                       </select>
                                    </td>
                                    <td>
                                       <input type="text" name="date[]" id="date" class="form-control">
                                    </td>
                                 
                                    <td>
                                    <input type="text" name="invoice_no[]" id="invoice_no" class="form-control text-center">
                                    </td>
                                    <td>
                                      
                                       <input type="text" name="invoice_date[]" id="invoice_date" class="form-control text-center">
                                    </td>

                                    {{--<td class="text-center">
                                       <input type="text" name="consignment_of[]" id="consignment_of" class="form-control text-center">
                                    </td>--}}

                                    {{--<td class="text-center">
                                       <input type="text" name="buyer[]" class="form-control text-center">
                                    </td>--}}

                                    <td class="text-center">
                                       <input type="text" name="amount[]" id="amount" class="form-control text-center">
                                    </td>
                                    <td class="d-flex">
                                       <div class="btn btn-primary" onclick="addRow()">+</div>
                                       <div class="btn btn-danger" onclick="removeRow(this)">-</div>
                                    </td>
                                 </tr>
                              </tbody>
                              <tbody>
                                 <tr>
                                    <td colspan="5" style="text-align:right"><b>Total Amount:</b></td>
                                    <td id="grand_total_cell" class="text-center">
                                       <span id="grand_total">0</span>
                                       <input type="hidden" name="grand_total" id="grand_total_input" value="0">
                                    </td>
                                 </tr>
                              </tbody>

                           </table>
                           <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>



<script>
   //Get exporter ways billTo, name, address
   function getExporter() {
       var id = $('#exporter_id').val();
       $.ajax({
           url: '/export-forwarding-bill/get-exporter/' + id,
           type: 'GET'
       })
       .done(function(response) {
           $('#bill_to').val(response.bill_to);
           $('#exporter_name').val(response.exporter_name);
           $('#exporter_address').val(response.exporter_address);
       })
       .fail(function(jqXHR, textStatus, errorThrown) {
           console.error('Error fetching exporter details:', textStatus, errorThrown);
       });
   }
   

   //Get bill no ways get amount
   function getAmount(selectElement) {
       var id = $(selectElement).val();
       $.ajax({
         url: '/export-forwarding-bill/get-grand-total/' + id,
           type: 'GET'
       })
       .done(function(response) {
           var invoice_no = response.invoice_no;
           var invoice_date = response.invoice_date;
           var date = response.date;
           var amount = response.amount || 0;
           $(selectElement).closest('tr').find('input[name="date[]"]').val(date);
           $(selectElement).closest('tr').find('input[name="invoice_no[]"]').val(invoice_no);
           $(selectElement).closest('tr').find('input[name="invoice_date[]"]').val(invoice_date);
           $(selectElement).closest('tr').find('input[name="amount[]"]').val(amount);
           updateGrandTotal();
       })
       .fail(function(jqXHR, textStatus, errorThrown) {
           console.error('Error fetching grand total:', textStatus, errorThrown);
       });
   }
   
   function addRow() {
       var tableBody = $('#table-body');
       var newRow = $('<tr></tr>');
   
       newRow.append('<td class="text-center">' + (tableBody.children('tr').length + 1) + '</td>');
       newRow.append('<td>' + generateExportBillsOptions() + '</td>');
       newRow.append('<td class="text-center"><input type="text" name="date[]" class="form-control text-center"></td>');
       newRow.append('<td><input type="text" name="invoice_no[]" class="form-control text-center"></td>');
       newRow.append('<td><input type="text" name="invoice_date[]" class="form-control text-center"></td>');
      //  newRow.append('<td class="text-center"><input type="text" name="buyer[]" class="form-control text-center"></td>');
       newRow.append('<td class="text-center"><input type="text" name="amount[]" class="form-control text-center"></td>');
       newRow.append('<td class="d-flex"><div class="btn btn-primary" onclick="addRow()">+</div><div class="btn btn-danger" onclick="removeRow(this)">-</div></td>');
   
       tableBody.append(newRow);
       newRow.find('.select2').select2();
       updateAmounts();
       updateGrandTotal();
   }
   
   
   function updateAmounts() {
       $('#table-body tr').each(function () {
           var row = $(this);
           var billNo = row.find('select[name="bill_no[]"]').val();
           var amountInput = row.find('input[name="amount[]"]');
           getAmount(billNo, amountInput);
       });
   }
   
   function generateExportBillsOptions() {
   var optionsHtml = '<select name="bill_no[]" class="form-control select2" onchange="getAmount(this)">';
   optionsHtml += '<option value=""></option>';
   @foreach ($exportBills as $item)
       optionsHtml += '<option value="{{ $item->id }}">{{ $item->bill_no }}</option>';
   @endforeach
   optionsHtml += '</select>';
   return optionsHtml;
   }
   
   
   function removeRow(button) {
       var rowToRemove = $(button).closest('tr');
       rowToRemove.remove();
       updateGrandTotal();
   }
   

   function updateGrandTotal() {
   var grandTotalSum = 0;

   $('#table-body tr').each(function () {
      var amountInput = $(this).find('input[name="amount[]"]');
      var amount = parseFloat(amountInput.val()) || 0;
      grandTotalSum += amount;
   });

   $('#grand_total').text(grandTotalSum.toFixed(2));
   $('#grand_total_input').val(grandTotalSum.toFixed(2));
}

</script>


@endsection