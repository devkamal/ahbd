@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <!--breadcrumb-->
      <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
         <div class="breadcrumb-title pe-3">Importer</div>
         <div class="ps-3">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mb-0 p-0">
                  <li class="breadcrumb-item"><a href="javascript:;"><i class="bx bx-home-alt"></i></a>
                  </li>
                  <li class="breadcrumb-item active" aria-current="page">All Importer</li>
               </ol>
            </nav>
         </div>
      </div>
      <!--end breadcrumb-->

     <div class="d-flex justify-content-between">
            <h6 class="mb-0 text-uppercase">Importer List</h6>
            <a class="mb-0 text-uppercase btn btn-primary btn-sm"
            data-bs-toggle="modal" data-bs-target="#addImporter">Add Importer</a>
     </div>
    
      <hr>
      <div class="card">
         <div class="card-body">
            <div class="table-responsive">
               <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap5">
                 
                  <div class="row">
                     <div class="col-sm-12">
                        <table id="example" class="table table-striped table-bordered dataTable" style="width: 100%;" role="grid" aria-describedby="example_info">
                           <thead>
                              <tr role="row">
                                 <th>Sl</th>
                                 <th> Name</th>
                                 <th> Phone</th>
                                 <th> Email</th>
                                 <th> Bill To</th>
                                 <th> Address</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              @foreach ($allData as $key => $item)
                                    <tr role="row" class="odd">
                                          <td>{{ $key+1 }}</td>
                                          <td>{{ $item->importer_name }}</td>
                                          <td>{{ $item->importer_phone }}</td>
                                          <td>{{ $item->importer_email }}</td>
                                          <td>{{ $item->bill_to }}</td>
                                          <td>{!! Str::words($item->importer_address, 3, ' ...') !!}</td>
                                          <td>
                                                <a data-bs-toggle="modal" data-bs-target="#editImporter{{$item->id}}" title="edit" class="btn btn-primary btn-sm">Eidt</a>
                                                <a href="{{ route('delete_importer',$item->id) }}" id="delete" title="delete" class="btn btn-danger btn-sm">Delete</a>
                                          </td>
                                    </tr>
                                    
                                    <!-- Edit Coupon Modal -->
                                    <div class="modal fade" id="editImporter{{$item->id}}" tabindex="-1" style="display: none;" aria-hidden="true">
                                          <div class="modal-dialog modal-lg modal-dialog-centered">
                                                <div class="modal-content">
                                                      <div class="modal-header">
                                                            <h5 class="modal-title text-dark">Edit Importer</h5>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                      </div>
                                                            <form action="{{route('update_importer',$item->id)}}" method="post">
                                                                  @csrf

                                                                  <div class="modal-body text-dark">

                                                                  <div class="col-md-12">
                                                                        <label for="name"> Name</label>
                                                                        <input type="text" name="importer_name" value="{{$item->importer_name}}" id="name" class="form-control">
                                                                  </div>

                                                                  <div class="col-md-12">
                                                                        <label for="name"> Phone</label>
                                                                        <input type="text" name="importer_phone" value="{{$item->importer_phone}}" id="importer_phone" class="form-control">
                                                                  </div>

                                                                  <div class="col-md-12">
                                                                        <label for="name"> Email</label>
                                                                        <input type="text" name="importer_email" value="{{$item->importer_email}}" id="importer_email" class="form-control">
                                                                  </div>

                                                                  <div class="col-md-12">
                                                                        <label for="name"> Address</label>
                                                                        <input type="text" name="importer_address" value="{{$item->importer_address}}" id="importer_address" class="form-control">
                                                                  </div>

                                                                  <div class="col-md-12 my-2">
                                                                        <label for="name">Bill To</label>
                                                                        <input name="bill_to" id="bill_to" class="form-control"  value="{{$item->bill_to}}">
                                                                  </div>

                                                                  </div>
                                                                  <div class="modal-footer">
                                                                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-dark" id="updateCoupon">Update</button>
                                                                  </div>
                                                            </form>
                                                </div>
                                          </div>
                                    </div>

                              @endforeach
                           </tbody>
                        </table>
                        {{ $allData->links('pagination_link') }}
                     </div>
                  </div>
                 
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<!-- Add Coupon Modal -->
<div class="modal fade" id="addImporter" tabindex="-1" style="display: none;" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                  <div class="modal-header">
                        <h5 class="modal-title text-dark">Add Importer</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
               <form action="{{ route('store_importer') }}" method="post">
                  @csrf
                  <div class="modal-body text-dark">

                    <div class="col-md-12">
                        <label for="name">Importer Name</label>
                        <input type="text" name="importer_name" class="form-control" placeholder="Enter importer Name">
                        @error('importer_name')
                              <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>

                    <div class="col-md-12 my-2">
                        <label for="name">Importer Phone</label>
                        <input type="text" name="importer_phone" class="form-control" placeholder="Enter importer Phone">
                        @error('importer_phone')
                              <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>

                    <div class="col-md-12">
                        <label for="name">Importer Email</label>
                        <input type="text" name="importer_email" class="form-control" placeholder="Enter importer Email">
                        @error('importer_email')
                              <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>

                    <div class="col-md-12 my-2">
                        <label for="name">Importer Address</label>
                        <textarea name="importer_address" id="importer_address" class="form-control"  placeholder="Enter importer Address"></textarea>
                        @error('importer_address')
                              <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>

                    <div class="col-md-12 my-2">
                        <label for="name">Bill To</label>
                        <input name="bill_to" id="bill_to" class="form-control"  placeholder="bill to">
                        @error('bill_to')
                              <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>

                   </div>
                   <div class="modal-footer">
                         <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                         <button type="submit" class="btn btn-dark">Save changes</button>
                   </div>
               </form>
            </div>
      </div>
</div>

 
@endsection
