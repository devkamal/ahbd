<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('export_forwarding_bills', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('exporter_id');
            $table->string('bill_to');
            $table->string('exporter_name');
            $table->text('exporter_address');
            $table->string('sub')->nullable();
            $table->text('message')->nullable();
            $table->decimal('grand_total', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('export_forwarding_bills');
    }
};
