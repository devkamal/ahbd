<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('export_bills', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->unsignedBigInteger('exporter_id');
            $table->string('exporter_address')->nullable();
            $table->string('ip_no')->nullable();
            $table->date('invoice_date')->nullable();
            $table->string('invoice_no')->nullable();
            $table->string('consignment_of')->nullable();
            $table->date('be_date')->nullable();
            $table->string('be_no')->nullable();
            $table->date('lc_date')->nullable();
            $table->string('lc_no')->nullable();
            $table->string('rot_no')->nullable();
            $table->string('export_ss')->nullable();
            $table->string('bl_no')->nullable();
            $table->date('bl_date')->nullable();
            $table->unsignedBigInteger('category_id');
            $table->string('bill_no')->unique();
            $table->decimal('paid_amount', 10, 2)->nullable();
            $table->decimal('due_amount', 10, 2)->nullable();
            $table->decimal('grand_total', 10, 2)->nullable();
            $table->string('usd_value')->nullable();
            $table->string('bdt_rate')->nullable();
            $table->string('invoice_value')->nullable();

            $table->decimal('agency_fixed', 10, 2)->nullable();
            $table->decimal('agency_value', 10, 2)->nullable();
            $table->string('agency_percent')->nullable();

            $table->foreign('exporter_id')->references('id')->on('exporters')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories');
        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('export_bills');
    }
};
