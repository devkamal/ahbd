<?php

namespace App\Http\Controllers\Backend;

use App\Models\Importer;
use App\Models\ImportBill;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ImportForwardingBill;
use App\Models\ImportForwardingBillDetail;

class ImportForwardingBillController extends Controller
{
    public function view(){
        $allData = ImportForwardingBill::orderBy('id','desc')->paginate(10);
        return view('backend.forwardingbill.import.view_import_forwarding_bill',compact('allData'));
       }
    
    public function add(){
    $importers = Importer::all();
    $importBills = ImportBill::all();
    return view('backend.forwardingbill.import.add_import_forwarding_bill',compact('importers','importBills'));
    }


    public function store(Request $request)
    {
        // dd($request->all());

        $importForwardingBill = ImportForwardingBill::create([
            'importer_id' => $request->input('importer_id'),
            'bill_to' => $request->input('bill_to'),
            'importer_name' => $request->input('importer_name'),
            'importer_address' => $request->input('importer_address'),
            'sub' => $request->input('sub'),
            'message' => $request->input('message'),
            'grand_total' => $request->input('grand_total'),
        ]);

        // Loop through the details and create import Forwarding Bill Details
        foreach ($request->input('bill_no') as $index => $billNo) {
            ImportForwardingBillDetail::create([
                'import_forwarding_bill_id' => $importForwardingBill->id,
                'bill_no' => $billNo,
                'bl_no' => $request->input('bl_no')[$index],
                'ip_no' => $request->input('ip_no')[$index],
                'buyer' => $request->input('buyer')[$index],
                'amount' => $request->input('amount')[$index],
            ]);
        }

        $notification = array(
        'message' => 'ImportForwardingBill Inserted Successfully',
            'alert-type' => 'success'
        );

        return redirect()->route('view_import_forwarding_bill')->with($notification); 
      }
    
       public function edit($id){
       
       }

       public function detailsView($id){
        $details = ImportForwardingBill::with('details')->findOrFail($id);
        $importers = Importer::all();
        return view('backend.forwardingbill.import.details_forwarding_bill',compact('details','importers'));
       }

       public function pdfImportForwardingBill($id){
        $pdfbill = ImportForwardingBill::with('details')->findOrFail($id);
        $importers = Importer::all();
        return view('backend.forwardingbill.import.pdf_forwarding_importbill',compact('pdfbill','importers'));
       }
       
    
       public function update(Request $request, $id){
    
       }

    
    public function delete($id){
       $product = ExportForwardingBill::findOrFail($id);
       ExportForwardingBill::findOrFail($id)->delete();
    
       $details = ExportForwardingBillDetail::where('export_forwarding_bill_id',$id)->get();
       foreach($details as $item){
           ExportForwardingBillDetail::where('export_forwarding_bill_id',$id)->delete();
       }
    
       $notification = array(
           'message' => 'Export Forwarding Bill Deleted Successfully',
           'alert-type' => 'success'
       );
    
       return redirect()->back()->with($notification);
    }

    //GEt json data from importer like name,address,billto
    public function GetImporter($id){
        $importer = Importer::find($id);

        if ($importer) {
            return response()->json([
                'bill_to' => $importer->bill_to, 
                'importer_name' => $importer->importer_name, 
                'importer_address' => $importer->importer_address
            ]);
        } else {
            return response()->json(['error' => 'Importer not found'], 404);
        }
    }

    public function GetGrandTotal($id){
        $grandTotal = ImportBill::find($id);

        if ($grandTotal) {
            return response()->json([
                'date' => $grandTotal->date,
                'lc_no' => $grandTotal->lc_no,
                'invoice_no' => $grandTotal->invoice_no,
                'invoice_date' => $grandTotal->invoice_date,
                'consignment_of' => $grandTotal->consignment_of,
                'amount' => $grandTotal->grand_total,
            ]);
        } else {
            return response()->json(['error' => 'GrandTotal not found'], 404);
        }
    }
}
