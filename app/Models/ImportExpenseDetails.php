<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImportExpenseDetails extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function importBill()
    {
        return $this->belongsTo(ImportBill::class);
    }
}
