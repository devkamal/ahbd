<?php

use App\Models\ExportBill;
use DaveChild\TextStatistics\NumberToWords;

  function bill_no(){
    $bill = ExportBill::orderBy('id','desc')->first();

    if($bill){
        $bill_no = (int)$bill->invoice_no + 1;
    } else {
        $bill_no = 1;
    }

    // Format the bill number with leading zeros
    $formatted_bill_no = str_pad($bill_no, 5, '0', STR_PAD_LEFT);

    return $formatted_bill_no;
}
  

// if (!function_exists('accounting_format')) {
//     function accounting_format($number, $decimals = 2, $decimalSeparator = '.', $thousandsSeparator = ',') {
//         // Format the number with accounting format
//         $formattedNumber = number_format($number, $decimals, $decimalSeparator, $thousandsSeparator);

//         // Add currency symbol or any other formatting you need
//         $formattedNumber = '$' . $formattedNumber;

//         return $formattedNumber;
//     }
// }


if (!function_exists('accounting_format')) {
    function accounting_format($number, $decimals = 2) {
        $formattedNumber = number_format($number, $decimals, '.', ',');
        return $formattedNumber;
    }
}


//****************** */ Amount to Words
function numberToWord($num = '')
{
    $num    = (string) ((int) $num);

    if ((int) ($num) && ctype_digit($num)) {
        $words  = array();

        $num    = str_replace(array(',', ' '), '', trim($num));

        $list1  = array(
            '', 'one', 'two', 'three', 'four', 'five', 'six', 'seven',
            'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen',
            'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
        );

        $list2  = array(
            '', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty',
            'seventy', 'eighty', 'ninety', 'hundred'
        );

        $list3  = array(
            '', 'thousand', 'million', 'billion', 'trillion',
            'quadrillion', 'quintillion', 'sextillion', 'septillion',
            'octillion', 'nonillion', 'decillion', 'undecillion',
            'duodecillion', 'tredecillion', 'quattuordecillion',
            'quindecillion', 'sexdecillion', 'septendecillion',
            'octodecillion', 'novemdecillion', 'vigintillion'
        );

        $num_length = strlen($num);
        $levels = (int) (($num_length + 2) / 3);
        $max_length = $levels * 3;
        $num    = substr('00' . $num, -$max_length);
        $num_levels = str_split($num, 3);

        foreach ($num_levels as $num_part) {
            $levels--;
            $hundreds   = (int) ($num_part / 100);
            $hundreds   = ($hundreds ? ' ' . $list1[$hundreds] . ' Hundred' . ($hundreds == 1 ? '' : 's') . ' ' : '');
            $tens       = (int) ($num_part % 100);
            $singles    = '';

            if ($tens < 20) {
                $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '');
            } else {
                $tens = (int) ($tens / 10);
                $tens = ' ' . $list2[$tens] . ' ';
                $singles = (int) ($num_part % 10);
                $singles = ' ' . $list1[$singles] . ' ';
            }
            $words[] = $hundreds . $tens . $singles . (($levels && (int) ($num_part)) ? ' ' . $list3[$levels] . ' ' : '');
        }
        $commas = count($words);
        if ($commas > 1) {
            $commas = $commas - 1;
        }

        $words  = implode(', ', $words);

        $words  = trim(str_replace(' ,', ',', ucwords($words)), ', ');
        if ($commas) {
            $words  = str_replace(',', ' and', $words);
        }

        // Add 'Taka Only' at the end
        $words .= ' Taka Only';

        return $words;
    } else if (!((int) $num)) {
        return 'Zero Taka Only';
    }
    return '';
}


