<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitumin_forwarding_bill_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('bitumin_forwarding_bill_id');
            $table->string('bill_no');
            $table->string('lc_no')->nullable();
            $table->string('consignment_of')->nullable();
            $table->string('buyer')->nullable();
            $table->decimal('amount', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitumin_forwarding_bill_details');
    }
};
