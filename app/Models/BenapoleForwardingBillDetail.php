<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BenapoleForwardingBillDetail extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function bill()
    {
        return $this->belongsTo(BenapoleForwardingBill::class, 'benapole_forwarding_bill_id');
    }
}
