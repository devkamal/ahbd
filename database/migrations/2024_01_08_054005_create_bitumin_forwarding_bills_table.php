<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitumin_forwarding_bills', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('importer_id');
            $table->string('bill_to');
            $table->string('importer_name');
            $table->text('importer_address');
            $table->string('sub')->nullable();
            $table->text('message')->nullable();
            $table->decimal('grand_total', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitumin_forwarding_bills');
    }
};
