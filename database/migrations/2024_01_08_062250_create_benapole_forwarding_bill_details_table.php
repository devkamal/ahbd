<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benapole_forwarding_bill_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('benapole_forwarding_bill_id');
            $table->string('bill_no');
            $table->string('invoice_date')->nullable();
            $table->string('consignment_of')->nullable();
            $table->string('buyer')->nullable();
            $table->decimal('amount', 10, 2);
            $table->string('invoice_no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benapole_forwarding_bill_details');
    }
};
