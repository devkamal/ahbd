<?php

namespace App\Http\Controllers\Backend;

use Image;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Product;
use App\Models\Category;
use App\Models\Multiimage;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
   public function view(){
    $allData = Product::orderBy('id','desc')->paginate(10);
    return view('backend.product.view_product',compact('allData'));
   }

   public function add(){
   $categories = Category::all();
   $vendors = User::where('status','active')->where('role','vendor')->get();
    return view('backend.product.add_product',compact('categories','vendors'));
   }

   public function VendorGetSubCategory($category_id){
        $subcat = SubCategory::where('category_id',$category_id)->orderBy('subcategory_name','ASC')->get();
        return response()->json($subcat);
   }// End Method 
   
   public function store(Request $request){

      $image = $request->file('thumbnail');
      $name_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
      Image::make($image)->resize(800,800)->save('upload/productimg/thambnail/'.$name_gen);
      $save_url = 'upload/productimg/thambnail/'.$name_gen;

      $product_id = Product::insertGetId([
          'category_id' => $request->category_id,
          'subcategory_id' => $request->subcategory_id,
          'name' => $request->name,
          'slug' => strtolower(str_replace(' ','-',$request->name)),

          'code' => $request->code,
          'qty' => $request->qty,
          'tags' => $request->tags,
          'size' => $request->size,
          'color' => $request->color,

          'price' => $request->price,
          'discount_price' => $request->discount_price,
          'short_description' => $request->short_description,
          'long_description' => $request->long_description, 

          'hot_deal' => $request->hot_deal,
          'feature' => $request->feature,
          'speacial_deal' => $request->speacial_deal,
          'speacial_offer' => $request->speacial_offer, 

          'thumbnail' => $save_url,
          'status' => 1,
          'created_at' => Carbon::now(), 

      ]);

      /// Multiple Image Upload From her //////

      $images = $request->file('image');
      foreach($images as $img){
          $make_name = hexdec(uniqid()).'.'.$img->getClientOriginalExtension();
          Image::make($img)->resize(800,800)->save('upload/productimg/multi-image/'.$make_name);
          $uploadPath = 'upload/productimg/multi-image/'.$make_name;

         Multiimage::insert([
            'product_id' => $product_id,
            'image' => $uploadPath,
            'created_at' => Carbon::now(), 
         ]); 
      } // end foreach

      /// End Multiple Image Upload From her //////

      $notification = array(
          'message' => 'Product Inserted Successfully',
          'alert-type' => 'success'
      );

      return redirect()->route('view_product')->with($notification); 
   }

   public function edit($id){
      $editData = Product::find($id);
      $categories = Category::all();
      $subcate = SubCategory::all();
      $multiimg = Multiimage::where('product_id',$id)->get();
      $vendors = User::where('status','active')->where('role','vendor')->get();
      return view('backend.product.edit_product',
      compact('editData','categories','vendors','subcate','multiimg'));
   }

   public function update(Request $request, $id){
      $product_id = Product::find($id)->update([
         'category_id' => $request->category_id,
         'subcategory_id' => $request->subcategory_id,
         'name' => $request->name,
         'slug' => strtolower(str_replace(' ','-',$request->name)),

         'code' => $request->code,
         'qty' => $request->qty,
         'tags' => $request->tags,
         'size' => $request->size,
         'color' => $request->color,

         'price' => $request->price,
         'discount_price' => $request->discount_price,
         'short_description' => $request->short_description,
         'long_description' => $request->long_description, 

         'hot_deal' => $request->hot_deal,
         'feature' => $request->feature,
         'speacial_deal' => $request->speacial_deal,
         'speacial_offer' => $request->speacial_offer, 
         'status' => 1,
         'created_at' => Carbon::now(), 
     ]);

      $notification = array(
         'message' => 'Product Update Successfully',
         'alert-type' => 'success'
      );

      return redirect()->route('view_product')->with($notification); 

   }

   //Main image update indivisually
   public function mainImgUpdate(Request $request, $mainimg_update){
      $oldimg = $request->oldimg;

      $image = $request->file('thumbnail');
      $name_gen = hexdec(uniqid()).'.'.$image->getClientOriginalExtension();
      Image::make($image)->resize(800,800)->save('upload/productimg/thambnail/'.$name_gen);
      $save_url = 'upload/productimg/thambnail/'.$name_gen;
      if(file_exists($oldimg)){
         unlink($oldimg);
      }

      Product::findOrFail($mainimg_update)->update([
         'thumbnail' => $save_url,
         'updated_at' => Carbon::now(),
      ]);

      $notification = array(
         'message' => 'Product Image Thambnail Updated Successfully',
         'alert-type' => 'success'
     );

     return redirect()->back()->with($notification); 
   }

   //Multi Image update
   public function multiImgUpdate(Request $request){

      $imgs = $request->image;

      foreach($imgs as $id => $img ){
          $imgDel = Multiimage::findOrFail($id);
       
          @unlink(public_path('upload/productimg/multi-image/'.$imgDel->image));

         $make_name = hexdec(uniqid()).'.'.$img->getClientOriginalExtension();
         Image::make($img)->resize(800,800)->save('upload/productimg/multi-image/'.$make_name);
         $uploadPath = 'upload/productimg/multi-image/'.$make_name;

         Multiimage::where('id',$id)->update([
            'image' => $uploadPath,
            'updated_at' => Carbon::now(),
         ]); 
      } // end foreach

       $notification = array(
          'message' => 'Product Multi Image Updated Successfully',
          'alert-type' => 'success'
      );

      return redirect()->back()->with($notification); 

  }// End Method 

  public function multiImgUDelete($id){
   $oldImg = Multiimage::findOrFail($id);
   unlink($oldImg->image);

   Multiimage::findOrFail($id)->delete();

   $notification = array(
       'message' => 'Product Multi Image Deleted Successfully',
       'alert-type' => 'success'
   );

   return redirect()->back()->with($notification);
  }

  public function delete($id){
   $product = Product::findOrFail($id);
   unlink($product->thumbnail);
   Product::findOrFail($id)->delete();

   $imges = Multiimage::where('product_id',$id)->get();
   foreach($imges as $img){
       unlink($img->image);
       Multiimage::where('product_id',$id)->delete();
   }

   $notification = array(
       'message' => 'Product Deleted Successfully',
       'alert-type' => 'success'
   );

   return redirect()->back()->with($notification);
  }

  public function GetInActive($id){
   $data = Product::find($id)->update(['status'=>0]);
   return response()->json($data);
  }

  public function GetActive($id){
   $data = Product::find($id)->update(['status'=>1]);
   return response()->json($data);
  }

  public function ProductDetails($id){
      $details = Product::findOrFail($id);
      $categories = Category::all();
      $subcate = SubCategory::all();
      $multiimg = Multiimage::where('product_id',$id)->get();
      $vendors = User::where('status','active')->where('role','vendor')->get();
      return view('backend.product.details_product',compact('details','categories','subcate','multiimg','vendors'));
  }

  public function StockProduct(){
   $product = Product::latest()->get();
   return view('backend.product.stock_product',compact('product'));
  }


}
