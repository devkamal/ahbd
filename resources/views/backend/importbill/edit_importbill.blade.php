@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <div class="row">
         <div class="col-xl-12 mx-auto">
           <div class="d-flex justify-content-between">
                <h6 class="mb-0 text-uppercase">Update Import Bill</h6>
                <h6><a href="{{ route('view_import_bill') }}">Back</a></h6>
           </div>
            <hr/>
            <div class="card">
               <div class="card-body">
                  <div class="p-4 border rounded">
                     <form action="{{ route('update_import_bill',$editData->id) }}" class="row g-3" method="post">
                        @csrf
                     

                        <div class="col-md-3">
                           <label for="date" class="form-label">Date </label>
                           <input type="date" class="form-control" name="date" id="date" value="{{ $editData->date }}">
                        </div>

                   
                        <div class="col-md-3">
                           <label for="importer_id" class="form-label">Importer name </label>
                           <select onchange="getAddress()" class="form-select" name="importer_id" id="importer_id" class="form-control">
                              <option selected disabled value="">Select Name</option>
                              @foreach ($importers as $item)
                                 <option value="{{ $item->id }}" {{ $item->id == $editData->importer_id ? 'selected' : ''}}>{{ $item->importer_name }}</option>
                              @endforeach
                           </select>
                        </div>
                        <div class="col-md-6">
                           <label for="address" class="form-label">Importer Address </label>
                           <input type="text" class="form-control" name="importer_address" id="importer_address" value="{{ $editData->importer_address }}">
                        </div>
               
                 
                        <div class="col-md-3">
                           <label for="invoice_date" class="form-label">Invoice Date </label>
                           <input type="date" class="form-control" name="invoice_date" id="invoice_date" value="{{ $editData->invoice_date }}">
                           
                        </div>
                        <div class="col-md-3">
                           <label for="invoice_no" class="form-label">Invoice No</label>
                           <input type="text" class="form-control" name="invoice_no" id="invoice_no" value="{{ $editData->invoice_no }}">
                           
                        </div>
                        <div class="col-md-3">
                           <label for="consignment_of" class="form-label">Consignment Of </label>
                           <input type="text" class="form-control" name="consignment_of" id="consignment_of" value="{{ $editData->consignment_of }}">
                           
                        </div>
                        <hr>
                 
               
                        <div class="col-md-3">
                           <label for="be_date" class="form-label">B/E Date </label>
                           <input type="date" class="form-control" name="be_date" id="be_date" value="{{ $editData->be_date }}">
                           
                        </div>
                        <div class="col-md-3">
                           <label for="be_no" class="form-label">B/E No</label>
                           <input type="text" class="form-control" name="be_no" id="be_no" value="{{ $editData->be_no }}">
                           
                        </div>
                        <div class="col-md-3">
                           <label for="lc_date" class="form-label">L/C Date </label>
                           <input type="date" class="form-control" name="lc_date" id="lc_date" value="{{ $editData->lc_date }}">
                           
                        </div>
                        <div class="col-md-3">
                           <label for="lc_no" class="form-label">L/C No</label>
                           <input type="text" class="form-control" name="lc_no" id="lc_no" value="{{ $editData->lc_no }}">
                           
                        </div>

                        <div class="col-md-3">
                           <label for="rot_no" class="form-label">ROT No</label>
                           <input type="text" class="form-control" name="rot_no" id="rot_no" value="{{ $editData->rot_no }}">
                           
                        </div>
                        <div class="col-md-3">
                           <label for="import_ss" class="form-label">EX. S.S </label>
                           <input type="text" class="form-control" name="import_ss" id="import_ss" value="{{ $editData->import_ss }}">
                           
                        </div>


                        {{--<div class="col-md-3">
                           <label for="bl_no" class="form-label">B/L No </label>
                           <input type="text" class="form-control" name="bl_no" id="bl_no" value="{{ $editData->bl_no }}">
                           
                        </div>
                        <div class="col-md-3">
                           <label for="bl_date" class="form-label">B/L Date</label>
                           <input type="date" class="form-control" name="bl_date" id="bl_date" value="{{ $editData->bl_date }}">
                           
                        </div>--}}


                     
                        <div class="col-md-3">
                            <label for="category_id" class="form-label">Bill Type </label>
                            <select class="form-select" name="category_id" id="category_id" class="form-control" onchange="getCategory()">
                                <option value="">Select Bill</option>
                                @foreach ($categories as $item)
                                    <option value="{{ $item->id }}" {{ $item->id == $editData->category_id ? 'selected' : ''}}>{{ $item->category_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-3">
                            <label for="bill_no" class="form-label">Bill No </label>
                            <input type="text" class="form-control" name="bill_no" id="bill_no" value="{{ $editData->bill_no }}" readonly>
                            
                        </div>

                        <div class="col-md-3">
                            <label for="usdValue" class="form-label">USD Value </label>
                            <input type="text" class="form-control" name="usd_value" id="usdValue" value="{{ $editData->usd_value }}">
                            
                        </div>
                        <div class="col-md-3">
                            <label for="bdtRate" class="form-label">BDT Rate </label>
                            <input type="text" class="form-control" name="bdt_rate" id="bdtRate" value="{{ $editData->bdt_rate }}">
                            
                        </div>
                        <div class="col-md-3">
                            <label for="invoice_value" class="form-label">Invoice Value (BDT) </label>
                            <input type="text" class="form-control" name="invoice_value" id="invoice_value" value="{{ $editData->invoice_value }}">
                            
                        </div>
                    
                        <hr>
                    

                                   <!-------------Start calculation table------------------->     
                                   <table class="table table-bordered" id="dynamic-table">
                                <thead class="bg-dark">
                                    <tr class="text-white text-center">
                                        <th scope="col">Particular of Expenses</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Unit</th>
                                        <th scope="col">Rate</th>
                                        <th scope="col">QTY</th>
                                        <th scope="col">Amount</th>
                                    </tr>
                                </thead>
                                @foreach ($editData->ImportExpenseDetails as $expense)
                                    <tr class="text-center">
                                          <td>{{ $expense->expense_name }} 
                                          <input type="hidden" name="expense_name[]" value="{{ $expense->expense_name }}">
                                          </td>
                                          <td>
                                             <input type="text" name="description[]" value="{{ $expense->description }}" class="form-control">
                                          </td>
                                          <td>
                                          <input type="text" name="unit[]" value="{{ $expense->unit }}" class="form-control">
                                          </td>
                                          <td>
                                          <input type="text" name="rate[]" value="{{ $expense->rate }}" class="form-control">
                                          </td>
                                          <td>
                                          <input type="text" name="qty[]" value="{{ $expense->qty }}" class="form-control">
                                          </td>
                                          <td>
                                          <input type="text" name="amount[]" value="{{ $expense->amount }}" class="form-control">
                                          </td>
                                    </tr>
                              @endforeach
                           <tbody class="text-center">
                              <tr>
                                 <td colspan="5" style="text-align:right"><b>Sub Total:</b></td>
                                 <td><b>
                                       <input type="text" name="sub_total" id="sub_total" value="{{ $editData->sub_total }}" class="form-control">
                                 </b></td>
                              </tr>
                              
                         
                                 @if(isset($editData->agency_fixed))
                                 <tr style="text-align:right">
                                       <td colspan="5" style="text-align:right"><b>Agency Commission(Fixed):</b></td>
                                       <td>
                                          <b>
                                             <input type="text" name="agency_fixed" value="{{ $editData->agency_fixed }}" class="form-control">
                                          </b>
                                       </td>
                                 </tr>
                              @elseif(isset($editData->agency_percent))
                                 <tr>
                                       <td colspan="5" style="text-align:right">
                                          <b>Agency Commission: </b>
                                          <b>
                                             <input type="text" id="agency_percent" name="agency_percent" value="{{ $editData->agency_percent }}%">
                                          </b>
                                       </td>

                                       <td colspan="5">
                                          <b>
                                             <input type="text" id="agency_value" name="agency_value" value="{{ $editData->agency_value }}" class="form-control">
                                          </b>
                                       </td>
                                 </tr>
                              @else
                                 <tr>
                                       <td colspan="5" style="text-align:right"><b>Agency Commission:</b></td>
                                       <td><b>No any Commission</b></td>
                                 </tr>
                              @endif
                              
                              
                              <tr>
                                 <td colspan="5" style="text-align:right"><b> Total Amount:</b></td>
                                 <td><b>
                                       <input type="text" id="grand_total" name="grand_total" value="{{ $editData->grand_total }}" class="form-control">
                                 </b></td>
                              </tr>
                           </tbody>
                            </table>                       
                            <!-------------End calculation table-------------------> 

                            </div>

                            <div class="col-md-12">
                            <div class="mt-4">
                            <label for="invoice_value" class="form-label">Notes:</label>
                            <textarea name="notes" id="notes" class="form-control" value="{{ $editData->notes }}">
                              {!! $editData->notes !!}
                            </textarea>
                            </div>
                        </div>
                         

                        <div class="col-12">
                           <button class="btn btn-primary" type="submit">Update form</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>



<!-- /////////////////////////////////////// -->

<script>

   function getAddress() {
        console.log('Function getAddress called');
        var id = $('#importer_id').val();
        $.ajax({
            url: '/importer/get-address/' + id,
            type: 'GET'
        })
        .done(function(response) {
            console.log('Success. Response:', response);
            $('#importer_address').val(response.importer_address);
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            console.error('Error fetching supplier mobile:', textStatus, errorThrown);
        });
    }

    function updateAmount(row) {
        var rate = parseFloat(row.querySelector('[name="rate[]"]').value) || 0;
        var qty = parseFloat(row.querySelector('[name="qty[]"]').value) || 0;
        var amountField = row.querySelector('[name="amount[]"]');
        var amount = rate * qty;

        // Set the value of the amount field
        amountField.value = amount.toFixed(2);

        // Recalculate grand total when amount is updated
        calculateGrandTotal();
    }

    function calculateGrandTotal() {
      var subTotal = 0;
      
      // Consider the original amount inputs
      var amountInputs = document.getElementsByName('amount[]');
      amountInputs.forEach(function (input) {
         subTotal += parseFloat(input.value) || 0;
      });

      // Display the sub_total value
      document.getElementById('sub_total').value = subTotal.toFixed(2);

      // Display the grand_total value
      document.getElementById('grand_total').value = subTotal.toFixed(2);

      calculateDueAmount();
   }

    document.addEventListener('input', function (event) {
        if (event.target && (event.target.name === 'rate[]' || event.target.name === 'qty[]')) {
            updateAmount(event.target.parentNode.parentNode);
        }
    });

    // Initial calculation
    calculateGrandTotal();
</script>

<!-- ////////////////////////////////////// -->

<script>
   document.addEventListener('DOMContentLoaded', function () {
       // Add input event listeners
       document.getElementById('usdValue').addEventListener('input', updateInvoiceValue);
       document.getElementById('bdtRate').addEventListener('input', updateInvoiceValue);
       document.getElementById('agency_value').addEventListener('input', calculateAgencyValue);
   
       // Initial calculation
       updateInvoiceValue();
       calculateAgencyValue();
   });
   
   function updateInvoiceValue() {
       // Get the input values
       var usdValue = parseFloat(document.getElementById('usdValue').value) || 0;
       var bdtRate = parseFloat(document.getElementById('bdtRate').value) || 0;
   
       // Calculate the invoice value
       var invoiceValue = usdValue * bdtRate;
   
       // Display the result
       document.getElementById('invoice_value').value = invoiceValue.toFixed(2);
       }
 

      function calculateAgencyValue() {
         // Get the input values
         var invoiceValue = parseFloat(document.getElementById('invoice_value').value) || 0;
         var agencyPercent = parseFloat(document.getElementById('agency_percent').value) || 0;

         // Calculate the agency value
         var agencyValue = (invoiceValue !== 0) ? (agencyPercent / 100) * invoiceValue : 0;

         // Display the result
         document.getElementById('agency_value').value = agencyValue.toFixed();
      }


</script>


@endsection