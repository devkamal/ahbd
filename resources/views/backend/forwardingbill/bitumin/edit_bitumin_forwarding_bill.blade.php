@extends('admin.master')
@section('content')
<div class="page-wrapper">
   <div class="page-content">
      <div class="row">
         <div class="col-lg-12">
            <div class="card mb-4">
               <div class="card text-left">
                  <div class="card-body">
                    <div class="d-flex justify-content-between">
                    <h4 class="card-title mb-3">Update Bitumin Forwarding Bill</h4>
                     <h4 class="card-title mb-3">
                        <a href="{{ route('view_bitumin_forwarding_bill') }}">Back</a>
                     </h4>
                    </div>

                     <form action="{{ route('update_bitumin_forwarding_bill',$editData->id) }}" method="POST">
                        @csrf
                        <div class="row">
                           <div class="col-lg-4">
                              <table class="table table-bordered">
                                 <tbody>
                                    <tr>
                                       <td colspan="2">
                                          <select name="importer_id" id="importer_id" class="form-control" onchange="getImporter()">
                                             <option value="">Select Importer</option>
                                             @foreach ($importers as $item)
                                                <option value="{{ $item->id }}" {{ $item->id == $editData->importer_id ? 'selected' : ''}}>{{ $item->importer_name }}</option>
                                              @endforeach
                                          </select>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Bill To</td>
                                       <td>
                                          <input type="text" name="bill_to" id="bill_to" class="form-control" value="{{ $editData->bill_to }}">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Importer Name</td>
                                       <td>
                                          <input type="text" name="importer_name" id="importer_name" class="form-control" value="{{ $editData->importer_name }}">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Importer Address</td>
                                       <td>
                                          <div class="form-group">
                                             <input type="text" class="form-control" name="importer_address" id="importer_address" value="{{ $editData->importer_address }}">
                                          </div>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                           <div class="col-lg-4"></div>
                           <div class="col-lg-4"></div>
                        </div>
                        <div>
                        <span>Sub: </span>
                        <input type="text" name="sub" class="form-control" id="sub" value="Submission of C&F Bill No:02"> <br>
                        <span>Message (Body): </span>
                        <input type="text" name="message" class="form-control" id="message" 
                        value="We are submitting here with the following C&F Bill and in this connection we would also request you to kindly
                        arrage payment of the same at an early date from your end"> <br>

                           <table class="table table-bordered">
                              <thead class="bg-dark">
                                 <tr class="text-white text-center">
                                    <th scope="col">Sl</th>
                                    <th scope="col">Bill No</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">LC No </th>
                                    <th scope="col">Particulars</th>
                                    {{--<th scope="col">Buyer</th>--}}
                                    <th scope="col">Amount</th>
                                 </tr>
                              </thead>
                              @foreach ($editData->details as $key => $expense)
                                    <tr class="text-center">
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $expense->bill_no }}</td>
                                    <td>{{ \Carbon\Carbon::parse($expense->created_at)->format('d-m-Y')}}</td>
                                    <td> {{ $expense->lc_no }}</td>
                                    <td> {{ $expense->consignment_of }}</td>
                                    <td>{{ $expense->amount }}</td>
                                    </tr>
                              @endforeach
                              <tbody>
                                 <tr>
                                    <td style="text-align:right" colspan="5"><b> Total Amount:</b></td>
                                    <td class="text-center"><b>{{ $editData->grand_total }}</b></td>
                                 </tr>
                              </tbody>

                           </table>
                           <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>



<script>
   //Get importer ways billTo, name, address
   function getImporter() {
       var id = $('#importer_id').val();
       $.ajax({
           url: '/import-forwarding-bill/get-importer/' + id,
           type: 'GET'
       })
       .done(function(response) {
           $('#bill_to').val(response.bill_to);
           $('#importer_name').val(response.importer_name);
           $('#importer_address').val(response.importer_address);
       })
       .fail(function(jqXHR, textStatus, errorThrown) {
           console.error('Error fetching importer details:', textStatus, errorThrown);
       });
   }
   
     //Get bill no ways get amount
     function getAmount(selectElement) {
       var id = $(selectElement).val();
       $.ajax({
           url: '/import-forwarding-bill/get-grand-total/' + id,
           type: 'GET'
       })
       .done(function(response) {
           var lc_no = response.lc_no;
           var consignment_of = response.consignment_of;
           var amount = response.amount || 0;
           $(selectElement).closest('tr').find('input[name="lc_no[]"]').val(lc_no);
           $(selectElement).closest('tr').find('input[name="consignment_of[]"]').val(consignment_of);
           $(selectElement).closest('tr').find('input[name="amount[]"]').val(amount);
           updateGrandTotal();
       })
       .fail(function(jqXHR, textStatus, errorThrown) {
           console.error('Error fetching grand total:', textStatus, errorThrown);
       });
   }
   
   function addRow() {
       var tableBody = $('#table-body');
       var newRow = $('<tr></tr>');
   
       newRow.append('<td class="text-center">' + (tableBody.children('tr').length + 1) + '</td>');
       newRow.append('<td>' + generateImportBillsOptions() + '</td>');
       newRow.append('<td><input type="text" name="lc_no[]" class="form-control"></td>');
       newRow.append('<td class="text-center"><input type="text" name="consignment_of[]" class="form-control"></td>');
      //  newRow.append('<td class="text-center"><input type="text" name="buyer[]" class="form-control"></td>');
       newRow.append('<td class="text-center"><input type="text" name="amount[]" class="form-control"></td>');
       newRow.append('<td class="d-flex"><div class="btn btn-primary" onclick="addRow()">+</div><div class="btn btn-danger" onclick="removeRow(this)">-</div></td>');
   
       tableBody.append(newRow);
       newRow.find('.select2').select2();
       updateAmounts();
       updateGrandTotal();
   }
   
   
   function updateAmounts() {
       $('#table-body tr').each(function () {
           var row = $(this);
           var billNo = row.find('select[name="bill_no[]"]').val();
           var amountInput = row.find('input[name="amount[]"]');
           getAmount(billNo, amountInput);
       });
   }
   
   function generateImportBillsOptions() {
   var optionsHtml = '<select name="bill_no[]" class="form-control select2" onchange="getAmount(this)">';
   optionsHtml += '<option value=""></option>';
   @foreach ($importBills as $item)
       optionsHtml += '<option value="{{ $item->id }}">{{ $item->bill_no }} / {{ \Carbon\Carbon::parse($item->date)->format('d/m/Y')}}</option>';
   @endforeach
   optionsHtml += '</select>';
   return optionsHtml;
   }
   
   
   function removeRow(button) {
       var rowToRemove = $(button).closest('tr');
       rowToRemove.remove();
       updateGrandTotal();
   }
   

   function updateGrandTotal() {
   var grandTotalSum = 0;

   $('#table-body tr').each(function () {
      var amountInput = $(this).find('input[name="amount[]"]');
      var amount = parseFloat(amountInput.val()) || 0;
      grandTotalSum += amount;
   });

   $('#grand_total').text(grandTotalSum.toFixed(2));
   $('#grand_total_input').val(grandTotalSum.toFixed(2));
}

</script>


@endsection