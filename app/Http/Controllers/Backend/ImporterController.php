<?php

namespace App\Http\Controllers\Backend;

use Carbon\Carbon;
use App\Models\Importer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImporterController extends Controller
{
    public function view(){
        $allData = Importer::orderBy('id','desc')->paginate(5);

        return view('backend.importer.view_importer',compact('allData'));
    }

    public function store(Request $request){
        $validateData = $request->validate([
            'importer_name'  => 'required',
        ]);

        $inputData = new Importer();
        $inputData->importer_name    = $request->importer_name;
        $inputData->importer_phone   = $request->importer_phone;
        $inputData->importer_email   = $request->importer_email;
        $inputData->importer_address = $request->importer_address;
        $inputData->bill_to = $request->bill_to;
        $inputData->created_at = Carbon::now();
        $inputData->save();

        $notification = array(
            'message' => 'Importer added successfully.',
            'alert-type' => 'success',
        );
        return redirect()->back()->with($notification);
    }

    public function update(Request $request, $id){
        $updateData = Importer::findOrFail($id);
        $updateData->importer_name    = $request->importer_name;
        $updateData->importer_phone   = $request->importer_phone;
        $updateData->importer_email   = $request->importer_email;
        $updateData->importer_address = $request->importer_address;
        $updateData->bill_to = $request->bill_to;
        $updateData->updated_at = Carbon::now();
        $updateData->update();

        $notification = array(
            'message' => 'Importer update successfully.',
            'alert-type' => 'success',
        );
        return redirect()->back()->with($notification);
    }

    public function delete($id){
        Importer::findOrFail($id)->delete();
        $notification = array(
            'message' => 'Importer remove successfully.',
            'alert-type' => 'success',
        );
        return redirect()->back()->with($notification);
    }

    //Get address name ways
    public function GetAddress($id)
    {
        $supplier = Importer::find($id);

        if ($supplier) {
            return response()->json(['importer_address' => $supplier->importer_address]);
        } else {
            return response()->json(['error' => 'Supplier not found'], 404);
        }
    }

}
