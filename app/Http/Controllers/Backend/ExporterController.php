<?php

namespace App\Http\Controllers\Backend;

use Carbon\Carbon;
use App\Models\Exporter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExporterController extends Controller
{
    public function view(){
        $allData = Exporter::orderBy('id','desc')->paginate(5);

        return view('backend.exporter.view_exporter',compact('allData'));
    }

    public function store(Request $request){
        $validateData = $request->validate([
            'exporter_name'  => 'required',
            'bill_to' => 'required',
        ]);

        $inputData = new Exporter();
        $inputData->exporter_name    = $request->exporter_name;
        $inputData->exporter_phone   = $request->exporter_phone;
        $inputData->exporter_email   = $request->exporter_email;
        $inputData->exporter_address = $request->exporter_address;
        $inputData->bill_to = $request->bill_to;
        $inputData->created_at = Carbon::now();
        $inputData->save();

        $notification = array(
            'message' => 'Exporter added successfully.',
            'alert-type' => 'success',
        );
        return redirect()->back()->with($notification);
    }

    public function update(Request $request, $id){
        $updateData = Exporter::findOrFail($id);
        $updateData->exporter_name    = $request->exporter_name;
        $updateData->exporter_phone   = $request->exporter_phone;
        $updateData->exporter_email   = $request->exporter_email;
        $updateData->exporter_address = $request->exporter_address;
        $updateData->bill_to = $request->bill_to;
        $updateData->updated_at = Carbon::now();
        $updateData->update();

        $notification = array(
            'message' => 'Exporter update successfully.',
            'alert-type' => 'success',
        );
        return redirect()->back()->with($notification);
    }

    public function delete($id){
        Exporter::findOrFail($id)->delete();
        $notification = array(
            'message' => 'Exporter remove successfully.',
            'alert-type' => 'success',
        );
        return redirect()->back()->with($notification);
    }

    //Get address name ways
    public function GetAddress($id)
    {
        $supplier = Exporter::find($id);

        if ($supplier) {
            return response()->json(['exporter_address' => $supplier->exporter_address]);
        } else {
            return response()->json(['error' => 'Supplier not found'], 404);
        }
    }

}
