<?php

namespace App\Http\Controllers\Backend;

use App\Models\Category;
use App\Models\Exporter;
use App\Models\ExportBill;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ExportForwardingBill;
use App\Models\ExportForwardingBillDetail;

class ExportForwardingBillController extends Controller
{
    public function view(){
        $allData = ExportForwardingBill::orderBy('id','desc')->paginate(10);
        return view('backend.forwardingbill.view_export_forwarding_bill',compact('allData'));
    }
    
    public function add(){
    $categories = Category::all();
    $exporters = Exporter::all();
    $exportBills = ExportBill::all();
    return view('backend.forwardingbill.add_export_forwarding_bill',compact('exporters','categories','exportBills'));
    }


    public function store(Request $request)
    {
        //dd($request->all());

        $exportForwardingBill = ExportForwardingBill::create([
            'exporter_id' => $request->input('exporter_id'),
            'bill_to' => $request->input('bill_to'),
            'exporter_name' => $request->input('exporter_name'),
            'exporter_address' => $request->input('exporter_address'),
            'sub' => $request->input('sub'),
            'message' => $request->input('message'),
            'grand_total' => $request->input('grand_total'),
        ]);

        // Loop through the details and create Export Forwarding Bill Details
        foreach ($request->input('bill_no') as $index => $billNo) {
            ExportForwardingBillDetail::create([
                'export_forwarding_bill_id' => $exportForwardingBill->id,
                'bill_no' => $billNo,
                'invoice_no' => $request->input('invoice_no')[$index],
                'invoice_date' => $request->input('invoice_date')[$index],
                'date' => $request->input('date')[$index],
                // 'buyer' => $request->input('buyer')[$index],
                'amount' => $request->input('amount')[$index],
            ]);
        }

        $notification = array(
        'message' => 'ExportForwardingBill Inserted Successfully',
            'alert-type' => 'success'
        );

        return redirect()->route('view_export_forwarding_bill')->with($notification); 
      }
    
       public function edit($id){
        $editData = ExportForwardingBill::with('details')->findOrFail($id);
        $categories = Category::all();
        $exporters = Exporter::all();
        $exportBills = ExportBill::all();
        return view('backend.forwardingbill.edit_export_forwarding_bill',
        compact('exporters','categories','exportBills','editData'));
       }

       public function detailsView($id){
        $details = ExportForwardingBill::with('details')->findOrFail($id);
        $exporters = Exporter::all();
        return view('backend.forwardingbill.details_forwarding_bill',compact('details','exporters'));
       }

       public function pdfExportForwardingBill($id){
        $pdfbill = ExportForwardingBill::with('details')->findOrFail($id);
        $exporters = Exporter::all();
        return view('backend.forwardingbill.pdf_forwarding_exportbill',compact('pdfbill','exporters'));
       }
       
    
       public function update(Request $request, $id){
        $data = ExportForwardingBill::find($id);
        $data->exporter_id = $request->exporter_id;
        $data->bill_to   = $request->bill_to;
        $data->exporter_name = $request->exporter_name;
        $data->exporter_address = $request->exporter_address;
        $data->sub = $request->sub;
        $data->message = $request->message;
        $data->update();
        
        $notification = array(
            'message' => 'ExportForwardingBill update successfully!',
            'alert-type' => 'success',
        );
        return redirect()->route('view_export_forwarding_bill')->with($notification);

       }

    
    public function delete($id){
       $product = ExportForwardingBill::findOrFail($id);
       ExportForwardingBill::findOrFail($id)->delete();
    
       $details = ExportForwardingBillDetail::where('export_forwarding_bill_id',$id)->get();
       foreach($details as $item){
           ExportForwardingBillDetail::where('export_forwarding_bill_id',$id)->delete();
       }
    
       $notification = array(
           'message' => 'Export Forwarding Bill Deleted Successfully',
           'alert-type' => 'success'
       );
    
       return redirect()->back()->with($notification);
    }

    //GEt json data from exporter like name,address,billto
    public function GetExporter($id){
        $exporter = Exporter::find($id);

        if ($exporter) {
            return response()->json([
                'bill_to' => $exporter->bill_to, 
                'exporter_name' => $exporter->exporter_name, 
                'exporter_address' => $exporter->exporter_address
            ]);
        } else {
            return response()->json(['error' => 'Exporter not found'], 404);
        }
    }

    public function GetGrandTotal($id){
        $grandTotal = ExportBill::find($id);

        if ($grandTotal) {
            return response()->json([
                'amount' => $grandTotal->grand_total,
                'invoice_no' => $grandTotal->invoice_no,
                'invoice_date' => $grandTotal->invoice_date,
                'date' => $grandTotal->date,
            ]);
        } else {
            return response()->json(['error' => 'GrandTotal not found'], 404);
        }
    }


}
