<?php

namespace App\Http\Controllers\Backend;

use App\Models\Category;
use App\Models\Importer;
use App\Models\ImportBill;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BenapoleForwardingBill;
use App\Models\BenapoleForwardingBillDetail;

class BenapoleForwardingBillController extends Controller
{
    public function view(){
        $allData = BenapoleForwardingBill::orderBy('id','desc')->paginate(10);
        return view('backend.forwardingbill.benapole.view_benapole_forwarding_bill',compact('allData'));
       }
    
    public function add(){
    $importers = Importer::all();
    $importBills = ImportBill::all();
    return view('backend.forwardingbill.benapole.add_benapole_forwarding_bill',compact('importers','importBills'));
    }


    public function store(Request $request)
    {
        //dd($request->all());

        $benapoleForwardingBill = BenapoleForwardingBill::create([
            'importer_id' => $request->input('importer_id'),
            'bill_to' => $request->input('bill_to'),
            'importer_name' => $request->input('importer_name'),
            'importer_address' => $request->input('importer_address'),
            'sub' => $request->input('sub'),
            'message' => $request->input('message'),
            'grand_total' => $request->input('grand_total'),
        ]);

        // Loop through the details and create import Forwarding Bill Details
        foreach ($request->input('bill_no') as $index => $billNo) {
            BenapoleForwardingBillDetail::create([
                'benapole_forwarding_bill_id' => $benapoleForwardingBill->id,
                'bill_no' => $billNo,
                'invoice_no' => $request->input('invoice_no')[$index],
                'invoice_date' => $request->input('invoice_date')[$index],
                'consignment_of' => $request->input('consignment_of')[$index],
                // 'buyer' => $request->input('buyer')[$index],
                'amount' => $request->input('amount')[$index],
            ]);
        }

        $notification = array(
        'message' => 'BenapoleForwardingBill Inserted Successfully',
            'alert-type' => 'success'
        );

        return redirect()->route('view_benapole_forwarding_bill')->with($notification); 
      }
    
      public function edit($id){
        $editData = BenapoleForwardingBill::with('details')->findOrFail($id);
        $categories = Category::all();
        $importers = Importer::all();
        $importBills = ImportBill::all();
        return view('backend.forwardingbill.benapole.edit_benapole_forwarding_bill',
        compact('importers','categories','importBills','editData'));
       }

       public function update(Request $request, $id){
        $data = BenapoleForwardingBill::find($id);
        $data->importer_id = $request->importer_id;
        $data->bill_to   = $request->bill_to;
        $data->importer_name = $request->importer_name;
        $data->importer_address = $request->importer_address;
        $data->sub = $request->sub;
        $data->message = $request->message;
        $data->update();
        
        $notification = array(
            'message' => 'BenapoleForwardingBill update successfully!',
            'alert-type' => 'success',
        );
        return redirect()->route('view_benapole_forwarding_bill')->with($notification);

       }


       public function detailsView($id){
        $details = BenapoleForwardingBill::with('details')->findOrFail($id);
        $importers = Importer::all();
        return view('backend.forwardingbill.benapole.details_forwarding_bill',compact('details','importers'));
       }

       public function pdfBenapoleForwardingBill($id){
        $pdfbill = BenapoleForwardingBill::with('details')->findOrFail($id);
        $importers = Importer::all();
        return view('backend.forwardingbill.benapole.pdf_forwarding_importbill',compact('pdfbill','importers'));
       }
       

    
    public function delete($id){
       $product = BenapoleForwardingBill::findOrFail($id);
       BenapoleForwardingBill::findOrFail($id)->delete();
    
       $details = BenapoleForwardingBillDetail::where('benapole_forwarding_bill_id',$id)->get();
       foreach($details as $item){
           BenapoleForwardingBillDetail::where('benapole_forwarding_bill_id',$id)->delete();
       }
    
       $notification = array(
           'message' => 'BenapoleForwardingBill Deleted Successfully',
           'alert-type' => 'success'
       );
    
       return redirect()->back()->with($notification);
    }

}
