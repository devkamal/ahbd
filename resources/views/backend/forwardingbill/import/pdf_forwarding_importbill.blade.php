<head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <!--favicon-->
      <link rel="icon" href="{{ asset('backend/assets/images/favicon-32x32.png') }}" type="image/png" />
      <!--plugins-->
      <link href="{{ asset('backend/assets/plugins/vectormap/jquery-jvectormap-2.0.2.css') }}" rel="stylesheet"/>
      <link href="{{ asset('backend/assets/plugins/simplebar/css/simplebar.css') }}" rel="stylesheet" />
      <link href="{{ asset('backend/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet" />
      <link href="{{ asset('backend/assets/plugins/metismenu/css/metisMenu.min.css') }}" rel="stylesheet" />
      <!-- loader-->
      <link href="{{ asset('backend/assets/css/pace.min.css') }}" rel="stylesheet" />
      <script src="{{ asset('backend/assets/js/pace.min.js') }}"></script>
      <!-- Bootstrap CSS -->
      <link href="{{ asset('backend/assets/css/bootstrap.min.css') }}" rel="stylesheet">
      <link href="{{ asset('backend/assets/css/app.css') }}" rel="stylesheet">
      <link href="{{ asset('backend/assets/css/icons.css') }}" rel="stylesheet">
      <!-- Theme Style CSS -->
      <link rel="stylesheet" href="{{ asset('backend/assets/css/dark-theme.css') }}" />
      <link rel="stylesheet" href="{{ asset('backend/assets/css/semi-dark.css') }}" />
      <link rel="stylesheet" href="{{ asset('backend/assets/css/header-colors.css') }}" />
       <!-- Toastr CSS -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
      <title>Dashboard</title>
      <!-- font-awesome -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!-- DataTable -->
      <link href="{{ asset('backend/assets/plugins/datatable/css/dataTables.bootstrap5.min.css') }}" rel="stylesheet" />
      <!-- DataTable-->
      <link href="{{ asset('backend/assets/plugins/input-tags/css/tagsinput.css') }}" rel="stylesheet" />
      <!-- Jquery-->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
   </head>
<style>
      .text-center.invoiceHeader h1 {
      font-size: 75px;
      font-weight: 600;
      font-family: cursive;
      }
      .text-center.invoiceHeader h2 {font-family: system-ui;}
      .invoiceDate h6 {
      border: 2px solid black;
      width: 40%;
      line-height: 35px;
      }
</style>

      <div class="card mb-4">
         <div class="card text-left">
            <div class="card-body">
                  <div class="text-center invoiceHeader">
                        <h1>Chowdhury Syndicate</h1>
                        <h2>Shipping Clearing & Forwarding Agent </h2>
                        <h6>950/B,AsadGonj,Chittagong, Phone:031-2858488,631613,622405 </h6>
                  </div>
                  <div class="row">
                     <table>
                        <tr class="text-center">
                           <td>
                             
                           </td>

                           <td>
                             
                           </td>

                           <td style="text-align:right;">
                              <h6>Date: {{ \Carbon\Carbon::parse($pdfbill->created_at)->format('d/m/Y')}}</h6>
                           </td>
                        </tr>
                     </table>
                      
                  </div>
                  <div class="row">
                        <div class="col-md-12">
                              <p>Bill To: {{ $pdfbill['importer']['bill_to']}}</p>
                              <p> {{ $pdfbill['importer']['importer_name'] }}</p>
                              <p> {{ $pdfbill['importer']['importer_address'] }}</p>
                              <p>Sub: {{ $pdfbill->sub }}</p>
                              <p>{{ $pdfbill->message }}</p>
                        </div>
                  </div>

               <div class="header_img mb-2">
                  <div class="invoice-masthead" style="margin-bottom: 10px;position:relative;">
                     <div class="invoice-text" style="text-align:center; float:none">

                           <label
                           <div class="card mb-4">
                              <div class="card text-left">
                                 <div class="card-body">
                                    <div>
                                        <!-------------Start calculation table------------------->     
                                       <table class="table table-bordered" id="dynamic-table">
                                          <thead class="bg-dark">
                                                <tr class="text-white text-center">
                                                   <th scope="col">SL</th>
                                                   <th scope="col">Bill No</th>
                                                   <th scope="col">BL No</th>
                                                   <th scope="col">IP No</th>
                                                   <th scope="col">Buyer</th>
                                                   <th scope="col">Amount</th>
                                                </tr>
                                          </thead>
                                          @foreach ($pdfbill->details as $key => $expense)
                                                <tr class="text-center">
                                                <td>{{ $key+1 }}</td>
                                                <td>{{ $expense->bill_no }}</td>
                                                <td>{{ $expense->bl_no }}</td>
                                                <td>{{ $expense->ip_no }}</td>
                                                <td>{{ $expense->buyer }}</td>
                                                <td>{{ $expense->amount }}</td>
                                                </tr>
                                          @endforeach
                                          <tbody>
                                             <tr>
                                             <p style="font-size:10px;margin-top:5px">Amount in Words : {{numberToWord($pdfbill->grand_total)}}</p>
                                                <td style="text-align:right" colspan="5"><b> Total Amount:</b></td>
                                                <td class="text-center"><b>{{ $pdfbill->grand_total }}</b></td>
                                             </tr>
                                          </tbody>
                                       </table>     
                                   
                                       <button onclick="window.print()">Print</button>                    
                                       <!-------------End calculation table------------------->    
                                    </div>
                                 </div>
                              </div>
                           </div>
                   
                        </h4>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
  

<script>
   $(document).ready(function(){
      window.print();
   });
</script>
      <!-- Bootstrap JS -->
      <script src="{{ asset('backend/assets/js/bootstrap.bundle.min.js') }}"></script>
      <!--plugins-->
      <script src="{{ asset('backend/assets/js/jquery.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/simplebar/js/simplebar.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/metismenu/js/metisMenu.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/chartjs/js/Chart.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/jquery.easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/sparkline-charts/jquery.sparkline.min.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/jquery-knob/excanvas.js') }}"></script>
      <script src="{{ asset('backend/assets/plugins/jquery-knob/jquery.knob.js') }}"></script>
      <!-- <script src="{{ asset('backend/assets/js/index.js') }}"></script> -->
       <!--Validate JS-->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>
      <!--app JS-->
      <script src="{{ asset('backend/assets/js/app.js') }}"></script>
      <!-- Toastr JS -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
      <!-- Tostr js -->
      <!--Datatable-->
      <script src="{{ asset('backend/assets/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>