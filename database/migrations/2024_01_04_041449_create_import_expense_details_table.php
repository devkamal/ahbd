<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_expense_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('import_bill_id');
            $table->foreign('import_bill_id')->references('id')->on('import_bills')->onDelete('cascade');
            $table->string('expense_name');
            $table->text('description')->nullable();
            $table->string('unit')->nullable();
            $table->decimal('rate', 10, 2)->nullable();
            $table->decimal('qty', 10, 2)->nullable();
            $table->decimal('amount', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_expense_details');
    }
};
