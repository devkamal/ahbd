<?php

namespace App\Http\Controllers\Backend;

use App\Models\Category;
use App\Models\Importer;
use App\Models\PriceNote;
use App\Models\ImportBill;
use Illuminate\Http\Request;
use App\Models\BituminImportBill;
use App\Http\Controllers\Controller;
use App\Models\BituminImportBillDetails;

class BituminImportBillController extends Controller
{
    public function view(){
        $allData = BituminImportBill::orderBy('id','desc')->paginate(10);
        return view('backend.bituminimportbill.view_bitumin_importbill',compact('allData'));
       }
    
       public function add(){
        $categories = Category::all();
        $importers = Importer::all();
        return view('backend.bituminimportbill.add_bitumin_importbill',compact('importers','categories'));
       }
       

       public function store(Request $request)
       {
            //dd($request->all());

            $validateData = $request->validate([
                'date' => 'required',
                'importer_id' => 'required',
                'category_id' => 'required',
            ]);

            $importBill = BituminImportBill::create([
               'date' => $request->date,
               'importer_id' => $request->importer_id,
               'importer_address' => $request->importer_address,
               'ip_no' => $request->ip_no,
               'invoice_date' => $request->invoice_date,
               'invoice_no' => $request->invoice_no,
               'consignment_of' => $request->consignment_of,
               'be_date' => $request->be_date,
               'be_no' => $request->be_no,
               'lc_date' => $request->lc_date,
               'lc_no' => $request->lc_no,
               'rot_no' => $request->rot_no,
               'import_ss' => $request->import_ss,
               'bl_no' => $request->bl_no,
               'bl_date' => $request->bl_date,
               'category_id' => $request->category_id,
               'bill_no' => $request->bill_no,

               'sub_total' => $request->sub_total,
               'grand_total' => $request->grand_total,
               'paid_amount' => $request->paid_amount,
               'due_amount' => $request->due_amount,

               'usd_value' => $request->usd_value,
               'bdt_rate' => $request->bdt_rate,
               'invoice_value' => $request->invoice_value,
               
               'agency_fixed' => $request->agency_fixed,
               'agency_value' => $request->agency_value,
               'agency_percent' => $request->agency_percent,
           ]);
   
           // Create ExpenseDetails
           $details = [];
           $count = count($request->expense_name);
       
           for ($i = 0; $i < $count; $i++) {
               $bituminImportBillDetail = new BituminImportBillDetails([
                   'expense_name' => $request->expense_name[$i],
                   'description' => $request->description[$i],
                   'rate' => $request->rate[$i],
                   'qty' => $request->qty[$i],
                   'amount' => $request->amount[$i],
               ]);
       
               // Associate ImportExpenseDetails with ImportBill
               $importBill->bituminImportBillDetails()->save($bituminImportBillDetail);
           }
       
           // Create PriceNote records for the entire BituminImportBill
           $notes = $request->input('notes');
           $prices = $request->input('price');
       
           foreach ($notes as $key => $note) {
               $details[] = [
                   'bitumin_import_bill_id' => $importBill->id,
                   'notes' => $note,
                   'price' => $prices[$key],
               ];
           }
       
           // Create PriceNote records using insert
           PriceNote::insert($details);
           
   
          
         $notification = array(
            'message' => 'Bitumin ImportBill Inserted Successfully',
               'alert-type' => 'success'
         );
   
         return redirect()->route('view_bitumin_import_bill')->with($notification); 
       }
    


       public function edit($id){
         $editData = BituminImportBill::with('BituminImportBillDetails')->findOrFail($id);
         $importers = Importer::all();
         $categories = Category::all();
         return view('backend.bituminimportbill.edit_bitumin_importbill',compact('editData','importers','categories'));
       }
       
    
    public function update(Request $request, $id)
       {
       // dd($request->all());

           $exportBill = BituminImportBill::findOrFail($id);
       
           $exportBill->update([
            'date' => $request->date,
            'importer_id' => $request->importer_id,
            'importer_address' => $request->importer_address,
            'ip_no' => $request->ip_no,
            'invoice_date' => $request->invoice_date,
            'invoice_no' => $request->invoice_no,
            'consignment_of' => $request->consignment_of,
            'be_date' => $request->be_date,
            'be_no' => $request->be_no,
            'lc_date' => $request->lc_date,
            'lc_no' => $request->lc_no,
            'rot_no' => $request->rot_no,
            'import_ss' => $request->import_ss,
            'bl_no' => $request->bl_no,
            'bl_date' => $request->bl_date,
            'category_id' => $request->category_id,
            'bill_no' => $request->bill_no,

            'notes' => $request->notes,

            'sub_total' => $request->sub_total,
            'grand_total' => $request->grand_total,
            'paid_amount' => $request->paid_amount,
            'due_amount' => $request->due_amount,

            'usd_value' => $request->usd_value,
            'bdt_rate' => $request->bdt_rate,
            'invoice_value' => $request->invoice_value,
            
            'agency_fixed' => $request->agency_fixed,
            'agency_value' => $request->agency_value,
            'agency_percent' => $request->agency_percent,
           ]);
       
           // Update or create ExpenseDetails
           $BituminImportBillDetails = [];
           foreach ($request->description as $key => $description) {
               $BituminImportBillDetails[] = [
                   'expense_name' => $request->expense_name[$key],
                   'description' => $request->description[$key],
                //    'unit' => $request->unit[$key],
                   'rate' => $request->rate[$key],
                   'qty' => $request->qty[$key],
                   'amount' => $request->amount[$key],
               ];
           }
       
           $exportBill->BituminImportBillDetails()->delete(); // Delete existing records
           $exportBill->BituminImportBillDetails()->createMany($BituminImportBillDetails);
       
           $notification = [
               'message' => 'Bitumin ImportBill Updated Successfully',
               'alert-type' => 'success',
           ];
       
           return redirect()->route('view_bitumin_import_bill')->with($notification);
    }
       


    public function detailsView($id){
        $details = BituminImportBill::with('BituminImportBillDetails')->findOrFail($id);
        $importers = Importer::all();
        $categories = Category::all();
        return view('backend.bituminimportbill.details_bitumin_importbill',compact('details','importers','categories'));
       }

    public function pdfBituminImportBill($id){
    $pdfbill = BituminImportBill::with('BituminImportBillDetails')->findOrFail($id);
    $importers = Importer::all();
    $categories = Category::all();
    return view('backend.bituminimportbill.pdf_bitumin_importbill',compact('pdfbill','importers','categories'));
    }
       
    
    // public function delete($id){
    //    $product = BituminImportBill::findOrFail($id);
    //    BituminImportBill::findOrFail($id)->delete();
    
    //    $details = BituminImportBillDetails::where('bitumin_import_bill_id',$id)->get();
    //    foreach($details as $item){
    //        BituminImportBillDetails::where('bitumin_import_bill_id',$id)->delete();
    //    }

    //    $details = PriceNote::where('bitumin_import_bill_id',$id)->get();
    //    foreach($details as $item){
    //        PriceNote::where('bitumin_import_bill_id',$id)->delete();
    //    }
    
    //    $notification = array(
    //        'message' => 'Bitumin Import Bill Deleted Successfully',
    //        'alert-type' => 'success'
    //    );
    
    //    return redirect()->back()->with($notification);
    // }


    public function delete($id){
           // Find the BituminImportBill record
        $bituminImportBill = BituminImportBill::findOrFail($id);

        // Delete related BituminImportBillDetails
        $bituminImportBill->bituminImportBillDetails()->delete();

        // Delete related PriceNotes
        $bituminImportBill->priceNotes()->delete();

        // Delete the BituminImportBill record
        $bituminImportBill->delete();
     
        $notification = array(
            'message' => 'Bitumin Import Bill Deleted Successfully',
            'alert-type' => 'success'
        );
     
        return redirect()->back()->with($notification);
     }

    


}
