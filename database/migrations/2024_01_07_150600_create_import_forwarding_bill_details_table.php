<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_forwarding_bill_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('import_forwarding_bill_id');
            $table->string('bill_no');
            $table->string('bl_no')->nullable();
            $table->string('ip_no')->nullable();
            $table->string('buyer')->nullable();
            $table->decimal('amount', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_forwarding_bill_details');
    }
};
